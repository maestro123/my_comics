package maestro.comics.finder;

import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by artyom on 8/11/14.
 */
public class ObjectFinder {

    public static final String TAG = ObjectFinder.class.getSimpleName();

    public static List<File> find(File root, String pattern) {
        ArrayList<File> out = new ArrayList<File>();
        recursive(root, out, pattern);
        Log.e(TAG, "out = " + (out != null ? out.size() : out.size()));
        return out;
    }

    private static void recursive(File root, ArrayList<File> out, String pattern) {
        File[] files = root.listFiles();
        if (files != null && files.length > 0) {
            for (File file : files) {
                if (file.isDirectory()) {
                    recursive(file, out, pattern);
                } else if (file.getName().matches(pattern)) {
                    out.add(file);
                }
            }
        }
    }

}
