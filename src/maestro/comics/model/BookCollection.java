package maestro.comics.model;

/**
 * Created by artyom on 8/13/14.
 */
public class BookCollection {

    public long Id;
    public String Title;

    public BookCollection(String title) {
        Title = title;
    }

    public BookCollection(long id, String title) {
        this(title);
        Id = id;
    }

}
