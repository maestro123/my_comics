package maestro.comics;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.dream.android.mim.ImageLoadObject;
import com.dream.android.mim.MIM;
import maestro.comics.finder.ObjectFinder;
import maestro.comics.ui.BaseFragment;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Artyom on 15.08.2014.
 */
public class FileManagerFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Object[]>,
        View.OnClickListener {

    public static final String TAG = FileManagerFragment.class.getSimpleName();

    private static final int LOADER_ID = TAG.hashCode();

    private static final String SAVED_HISTORY = "saved_history";
    private static final String PARAM_PATH = "param_path";

    private ArrayList<String> mHistory = new ArrayList<String>();

    private HorizontalScrollView mPathsScrollView;
    private LinearLayout mPathsParent;
    private RecyclerView mList;
    private FileAdapter mAdapter;
    private View mEmptyView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.files_fragment_view, null);
        mList = (RecyclerView) v.findViewById(R.id.list);
        mList.setLayoutManager(new GridLayoutManager(getActivity(), getResources().getInteger(R.integer.num_of_file_columns)));
        mPathsScrollView = (HorizontalScrollView) v.findViewById(R.id.files_paths_parent_scroll_view);
        mPathsParent = (LinearLayout) v.findViewById(R.id.files_paths_parent);
        mEmptyView = v.findViewById(R.id.empty_view);
        v.findViewById(R.id.empty_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startLoad();
            }
        });
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Utils.prepareEmptyView(this, R.string.retry);
        mList.setAdapter(mAdapter = new FileAdapter(getActivity()));
        if (savedInstanceState == null) {
            mHistory.add(Environment.getExternalStorageDirectory().getPath());
        } else {
            mHistory = savedInstanceState.getStringArrayList(SAVED_HISTORY);
        }
        startLoad();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mainActivity.attachOnBackPressedListener(TAG, this);
    }

    @Override
    public void onDetach() {
        mainActivity.detachOnBackPressedListener(TAG);
        super.onDetach();
    }

    @Override
    public boolean onBackPressed() {
        if (mHistory.size() > 1) {
            mHistory.remove(mHistory.size() - 1);
            startLoad();
            return true;
        }
        return false;
    }

    private void startLoad() {
        Bundle args = new Bundle(1);
        args.putString(PARAM_PATH, mHistory.get(mHistory.size() - 1));
        if (getLoaderManager().getLoader(LOADER_ID) != null) {
            getLoaderManager().restartLoader(LOADER_ID, args, this);
        } else {
            getLoaderManager().initLoader(LOADER_ID, args, this);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArrayList(SAVED_HISTORY, mHistory);
    }

    @Override
    public Loader<Object[]> onCreateLoader(int i, Bundle bundle) {
        animateView(mEmptyView, View.GONE);
        updatePaths();
        return new FilesLoader(getActivity(), bundle.getString(PARAM_PATH));
    }

    @Override
    public void onLoadFinished(Loader<Object[]> loader, Object[] mFiles) {
        if (mAdapter != null) {
            mAdapter.update(mFiles);
            if (mAdapter.getItemCount() == 0) {
                animateView(mEmptyView, View.VISIBLE);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Object[]> loader) {

    }

    private void updatePaths() {
        final String last = mHistory.get(mHistory.size() - 1);
        String[] allPaths = last.split("/");
        mPathsParent.removeAllViews();
        final int size = allPaths.length;
        for (int i = 0; i < size; i++) {
            addPath(allPaths[i], i == 0, i == size - 1);
        }
        mPathsScrollView.post(new Runnable() {
            @Override
            public void run() {
                mPathsScrollView.fullScroll(View.FOCUS_RIGHT);
            }
        });
    }

    private void addPath(String path, boolean first, boolean last) {
        TextView textView = new TextView(getActivity());
        textView.setTextSize(18);
        final int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
        textView.setPadding(padding, 0, padding, 0);
        textView.setText(path);
        textView.setTextColor(Color.WHITE);
        textView.setAlpha(last ? 1f : .5f);
        textView.setGravity(Gravity.CENTER | Gravity.LEFT);
        textView.setOnClickListener(this);
        textView.setTag(path);
        if (last) {
            textView.setTextColor(getResources().getColor(R.color.indicator_color));
        }
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mPathsParent.addView(textView, params);
    }

    @Override
    public void onClick(View v) {

    }

    private class FileAdapter extends RecyclerView.Adapter<FileAdapter.Holder> {

        private Object[] mItems;
        private LayoutInflater mInflater;

        public FileAdapter(Context context) {
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public void update(Object[] files) {
            mItems = files;
            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            return mItems != null ? mItems.length : 0;
        }

        public Object getItem(int i) {
            return mItems[i];
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup viewGroup, int i) {
            return new Holder(mInflater.inflate(R.layout.file_item_view, null));
        }

        @Override
        public void onBindViewHolder(Holder h, int position) {
            Object file = mItems[position];
            if (file instanceof MFile) {
                MFile mFile = (MFile) file;
                ImageLoadObject.cancel(h.Image);
                h.Image.setImageResource(R.drawable.ic_folder);
                h.Title.setText(mFile.getName());
            } else if (file instanceof BookFile) {
                BookFile bookFile = (BookFile) file;
                h.Title.setText(bookFile.MFile.getName());
                MIM.by(Utils.MIM_COMICS).to(h.Image, bookFile.MFile.getPath()).object(bookFile).async();
            }

            h.ReadProgress.setVisibility(View.GONE);
            h.Additional.setVisibility(View.GONE);

        }

        class Holder extends RecyclerView.ViewHolder {
            TextView Title;
            TextView Additional;
            TextView ReadProgress;
            ImageView Image;

            public Holder(View v) {
                super(v);
                Title = (TextView) v.findViewById(R.id.title);
                Image = (ImageView) v.findViewById(R.id.image);
                Additional = (TextView) v.findViewById(R.id.additional);
                ReadProgress = (TextView) v.findViewById(R.id.read_progress);
                v.setTag(this);
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Holder holder = (Holder) v.getTag();
                        Object file = getItem(holder.getAdapterPosition());
                        if (file instanceof MFile) {
                            File jFile = new File(((MFile) file).getPath());
                            if (jFile.isDirectory()) {
                                mHistory.add(jFile.getPath());
                                startLoad();
                            }
                        } else {
                            BookFile bookFile = (BookFile) file;
                            mainActivity.openBook(bookFile.MFile.getPath());
                        }
                    }
                });
            }
        }

    }

    public static final class FilesLoader extends AsyncTaskLoader<Object[]> {

        private Object root;

        public FilesLoader(Context context, Object root) {
            super(context);
            this.root = root;
        }

        @Override
        public Object[] loadInBackground() {
            MFile[] files = null;
            if (root != null) {
                files = Utils.collectMFiles(root);
            } else {
                files = Utils.toMFiles(ObjectFinder.find(Environment.getExternalStorageDirectory(),
                        Utils.COMICS_PATTERN));
            }
            if (files != null && files.length > 0) {
                ArrayList<Object> outFiles = new ArrayList<Object>();
                for (MFile file : files) {
                    BookFile bookFile = Utils.makeBookFile(file);
                    if (bookFile != null) {
                        outFiles.add(bookFile);
                    } else {
                        outFiles.add(file);
                    }
                }
                return outFiles.toArray();
            }
            return files;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }
    }

    @Override
    public Object getKey() {
        return TAG;
    }
}
