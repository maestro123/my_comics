package maestro.comics.ui;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import maestro.comics.R;

/**
 * Created by artyom on 8/13/14.
 */
public abstract class IndicatorFragment extends BaseFragment implements ViewPager.OnPageChangeListener {

    private PlayIndicator mIndicator;
    private ViewPager mPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.indicator_fragment_view, null);
        mPager = (ViewPager) v.findViewById(R.id.pager);
        mIndicator = (PlayIndicator) v.findViewById(R.id.indicator);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPager.setAdapter(getPagerAdapter());
        mIndicator.setViewPager(mPager);
        mIndicator.setOnPageChangeListener(this);
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    @Override
    public void onPageScrolled(int i, float v, int i2) {

    }

    @Override
    public void onPageSelected(int i) {

    }

    public abstract PagerAdapter getPagerAdapter();

    public void update() {
        if (mPager != null && mPager.getAdapter() != null) {
            mPager.getAdapter().notifyDataSetChanged();
            mIndicator.notifyDataSetChanged();
        }
    }

    protected final String getCurrentFragmentTag() {
        return "android:switcher:" + mPager.getId() + ":" + mPager.getCurrentItem();
    }


    protected int getCurrentPagerPosition() {
        return mPager.getCurrentItem();
    }

}
