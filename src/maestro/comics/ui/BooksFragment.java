package maestro.comics.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import maestro.comics.BookFile;
import maestro.comics.R;
import maestro.comics.Utils;
import maestro.comics.data.BookManager;
import maestro.comics.data.BooksAdapter;
import maestro.comics.dialogs.ConfirmDialog;
import maestro.comics.dialogs.OnDialogConfirmListener;
import maestro.comics.model.BookCollection;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by artyom on 8/12/14.
 */
public class BooksFragment extends BaseFragment implements BookManager.OnBookManagerEventListener,
        View.OnClickListener, OnDialogConfirmListener, CollectionSelectDialog.OnCollectionSelectListener {

    public static final String TAG = BooksFragment.class.getSimpleName();

    private static final String PARAM_COLLECTION_ID = "collection_id";
    private RecyclerView mList;
    private BooksAdapter mAdapter;
    private ProgressBar mProgress;
    private View mEmptyView;
    private BooksSelectionMode mSelectionMode;

    public static final BooksFragment makeInstance(long collectionId) {
        BooksFragment fragment = new BooksFragment();
        Bundle args = new Bundle(1);
        args.putLong(PARAM_COLLECTION_ID, collectionId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.main_view, null);
        mList = (RecyclerView) v.findViewById(R.id.list);
        mProgress = (ProgressBar) v.findViewById(R.id.progress);
        mEmptyView = v.findViewById(R.id.empty_view);
        v.findViewById(R.id.empty_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateAdapter();
                BookManager.getInstance().scanIfAllow();
            }
        });
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Utils.prepareEmptyView(this, R.string.retry);
        final int columnCount = getResources().getInteger(R.integer.num_of_columns);
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), columnCount);
        mList.setAdapter(mAdapter = new BooksAdapter(getActivity()));
        mList.setLayoutManager(gridLayoutManager);
        gridLayoutManager.setSpanSizeLookup(new BooksAdapter.BooksSpanSizeLookup(mAdapter, columnCount));
        mAdapter.setOnItemActionListener(new BooksAdapter.OnItemActionListener() {
            @Override
            public void OnItemClick(View v, int position) {
                BookFile file = mAdapter.getItem(position);
                mainActivity.openBook(file.MFile.getPath());
            }

            @Override
            public boolean onItemLongClick(View vm, int position) {
                ((AppCompatActivity) getActivity()).startSupportActionMode(mSelectionMode = new BooksSelectionMode(BooksFragment.this, mList, mAdapter, position));
                return true;
            }
        });
        updateAdapter();
    }

    @Override
    public Object getKey() {
        return TAG + getArguments() != null ? getArguments().getLong(PARAM_COLLECTION_ID, BookManager.NO_COLLECTION) : BookManager.NO_COLLECTION;
    }

    @Override
    public void onAttach(Activity activity) {
        BookManager.getInstance().attachListener(getKey(), this);
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        BookManager.getInstance().detachListener(getKey());
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateAdapter();
    }

    @Override
    public boolean onBackPressed() {
        if (mSelectionMode != null && mSelectionMode.finish()) {
            return true;
        }
        return super.onBackPressed();
    }

    @Override
    public void onBookManagerEvent(BookManager.BOOK_MANAGER_EVENT event, Object object) {
        switch (event) {
            case BOOK_ADD_TO_COLLECTION:
            case BOOK_REMOVED_FROM_COLLECTION:
                if (object instanceof BookCollection) {
                    if (((BookCollection) object).Id != getArguments().getLong(PARAM_COLLECTION_ID))
                        return;
                }
            case DB_LOADED:
            case SCAN_END:
                updateAdapter();
                break;
            case SCAN_START:
                if (mAdapter != null && mAdapter.getItemCount() == 0) {
                    animateView(mProgress, View.VISIBLE);
                    animateView(mEmptyView, View.GONE);
                }
                break;
        }
    }

    private void updateAdapter() {
        if (mAdapter != null) {
            BookFile[] bookFiles = BookManager.getInstance().getBooks(getArguments().getLong(PARAM_COLLECTION_ID));
            if (bookFiles != null && bookFiles.length > 0) {
                ArrayList<Object> objects = new ArrayList<>();
                if (getArguments().getLong(PARAM_COLLECTION_ID) == BookManager.NO_COLLECTION) {
                    ArrayList<BookFile> recent = BookManager.getInstance().getRecentBooks();
                    if (recent != null && recent.size() > 0) {
                        objects.add(R.string.recent);
                        final int size = recent.size();
                        for (int i = size - 1; i > -1 && i > size - 1 - getResources().getInteger(R.integer.num_of_columns); i--) {
                            objects.add(recent.get(i));
                        }
                        objects.add(R.string.all_comics);
                    }
                    objects.addAll(Arrays.asList(bookFiles));
                    mAdapter.update(objects.toArray(new Object[objects.size()]));
                } else {
                    mAdapter.update(bookFiles);
                }

                animateView(mProgress, View.GONE);
                animateView(mEmptyView, View.GONE);
//                mEmptyView.animate().alpha(0).start();
//                mProgress.animate().alpha(0).start();
            } else {
                mAdapter.update(null);
                if (BookManager.getInstance().getCurrentState() == BookManager.BOOK_MANAGER_STATE.IDLE) {
                    animateView(mProgress, View.GONE);
                    animateView(mEmptyView, View.VISIBLE);
//                    mProgress.animate().alpha(0).start();
//                    mEmptyView.animate().alpha(1f).start();
                } else {
                    animateView(mProgress, View.VISIBLE);
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.empty_button:
                updateAdapter();
                break;
        }
    }

    @Override
    public void onDialogConfirm(View view, Object object, int id) {
        if (id == ConfirmDialog.REMOVE_FROM_COLLECTIONS_ID) {
            new Thread() {
                @Override
                public void run() {
                    super.run();
                    BookManager.getInstance().removeFromCollection(mAdapter.getClonedSelectedObjects());
                }
            }.start();
        }
        if (mSelectionMode != null) {
            mSelectionMode.finish();
        }
    }

    @Override
    public void onCollectionSelect(final BookCollection collection) {
        new Thread() {
            @Override
            public void run() {
                super.run();
                BookManager.getInstance().addToCollection(mAdapter.getClonedSelectedObjects(), collection);
            }
        }.start();
        if (mSelectionMode != null) {
            mSelectionMode.finish();
        }
    }
}
