package maestro.comics.ui;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Toast;
import maestro.comics.Main;
import maestro.comics.R;
import maestro.comics.SVGInstance;
import maestro.comics.data.BooksAdapter;
import maestro.comics.dialogs.ConfirmDialog;

/**
 * Created by U1 on 23.06.2015.
 */
public class BooksSelectionMode implements ActionMode.Callback {

    public static final String TAG = BooksSelectionMode.class.getSimpleName();

    private Fragment mFragment;
    private ActionMode mActionMode;
    private Menu mMenu;
    private RecyclerView mList;
    private BooksAdapter mAdapter;
    private int mSelectedCount = 0;
    private boolean isAllSelected;

    public BooksSelectionMode(Fragment fragment, RecyclerView list, BooksAdapter adapter, int startPosition) {
        mFragment = fragment;
        mList = list;
        mAdapter = adapter;
        mAdapter.startSelection(this, list, startPosition);
    }

    public void setSelectedCount(int count) {
        mSelectedCount = count;
        if (mActionMode != null) {
            mActionMode.setTitle(String.valueOf(count));
        }
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        if (mFragment != null) {
            Window mWindow = mFragment.getActivity().getWindow();
            mWindow.findViewById(R.id.action_mode_bar).setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            if (mFragment.getActivity() instanceof Main) {
                ((Main) mFragment.getActivity()).setDrawerState(false);
            }
        }
        mActionMode = mode;
        mMenu = menu;
        mode.getMenuInflater().inflate(R.menu.selection_menu, menu);
        menu.findItem(R.id.select_all).setIcon(SVGInstance.get().getDrawable(R.raw.ic_select_all, Color.WHITE));
        menu.findItem(R.id.add_to_collection).setIcon(SVGInstance.get().getDrawable(R.raw.ic_add, Color.WHITE));
        menu.findItem(R.id.remove_from_collection).setIcon(SVGInstance.get().getDrawable(R.raw.ic_remove, Color.WHITE));
        mode.setTitle("" + mSelectedCount);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return true;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        if (item.getItemId() == R.id.select_all) {
            mAdapter.setAllSelected(mList, isAllSelected = !isAllSelected);
            return true;
        } else if (item.getItemId() == R.id.add_to_collection) {
            if (mAdapter.haveSelectedItems()) {
                new CollectionSelectDialog().show(mFragment.getChildFragmentManager(), CollectionSelectDialog.TAG);
            } else {
                Toast.makeText(mFragment.getActivity(), R.string.no_selected_items, Toast.LENGTH_SHORT).show();
            }
            return true;
        } else if (item.getItemId() == R.id.remove_from_collection) {
            if (mAdapter.haveSelectedItems()) {
                ConfirmDialog dialog = ConfirmDialog.makeInstance(mFragment.getString(R.string.remove_all_from_collections), ConfirmDialog.REMOVE_FROM_COLLECTIONS_ID);
                dialog.show(mFragment.getChildFragmentManager(), ConfirmDialog.TAG);
            } else {
                Toast.makeText(mFragment.getActivity(), R.string.no_selected_items, Toast.LENGTH_SHORT).show();
            }
            return true;
        }
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        if (mAdapter != null) {
            mAdapter.finishSelection(mList);
        }
        if (mFragment.getActivity() instanceof Main) {
            ((Main) mFragment.getActivity()).setDrawerState(true);
        }
        mActionMode = null;
        mMenu = null;
    }

    public boolean finish() {
        if (mActionMode != null) {
            isAllSelected = false;
            mActionMode.finish();
            return true;
        }
        return false;
    }
}
