package maestro.comics.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.dream.android.mim.ImageLoadObject;
import com.dream.android.mim.MIM;
import com.dream.android.mim.MIMBlurMaker;
import com.dream.android.mim.MIMResourceMaker;
import maestro.comics.R;
import maestro.comics.Typefacer;
import maestro.comics.Utils;

import java.util.ArrayList;

/**
 * Created by Artyom on 12.08.2014.
 */
public class NavigationFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    public static final String TAG = NavigationFragment.class.getSimpleName();

    public static final int ACTION_MAIN = 0;
    public static final int ACTION_COLLECTIONS = 1;
    public static final int ACTION_SETTINGS = 2;
    public static final int ACTION_FILES = 3;

    private ListView mList;
    private ActionItemsAdapter mAdapter;
    private ArrayList<ActionItem> mItems = new ArrayList<ActionItem>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.navigation_view, null);
        mList = (ListView) v.findViewById(R.id.navigation_list);
        mList.setOnItemClickListener(this);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        prepareItems();
        mList.setAdapter(mAdapter = new ActionItemsAdapter(getActivity()));
//        MIM.by(Utils.MIM_COMICS).of("cover_back", String.valueOf(R.drawable.ic_icon_web)).maker(new MIMResourceMaker()).postMaker(new MIMBlurMaker(25)).listener(new ImageLoadObject.OnImageLoadEventListener() {
//            @Override
//            public void onImageLoadEvent(IMAGE_LOAD_EVENT event, ImageLoadObject loadObject) {
//                if (event == IMAGE_LOAD_EVENT.FINISH) {
//                    ((ImageView) getView().findViewById(R.id.background_image)).setImageDrawable((Drawable) loadObject.getResultObject());
//                }
//            }
//        }).async();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        ActionItem item = mAdapter.getItem(i);
        mainActivity.openFragmentById(item.ActionId);
    }

    final void prepareItems() {
        mItems.add(new ActionItem(ACTION_MAIN, getString(R.string.main), R.drawable.ic_home));
        mItems.add(new ActionItem(ACTION_COLLECTIONS, getString(R.string.collections), R.drawable.ic_collections));
        mItems.add(new ActionItem(ACTION_FILES, getString(R.string.files), R.drawable.ic_storage));
//        mItems.add(new ActionItem(ACTION_SETTINGS, getString(R.string.settings), R.drawable.ic_settings));
    }

    private final class ActionItemsAdapter extends BaseAdapter {

        private Context mContext;
        private LayoutInflater mInflater;

        public ActionItemsAdapter(Context context) {
            mContext = context;
            mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mItems != null ? mItems.size() : 0;
        }

        @Override
        public ActionItem getItem(int i) {
            return mItems.get(i);
        }

        @Override
        public long getItemId(int i) {
            return mItems.get(i).ActionId;
        }

        @Override
        public View getView(int position, View v, ViewGroup viewGroup) {
            Holder h;
            if (v == null) {
                h = new Holder();
                v = mInflater.inflate(R.layout.navigation_item_view, null);
                h.Icon = (ImageView) v.findViewById(R.id.icon);
                h.Title = (TextView) v.findViewById(R.id.title);
                h.Additional = (TextView) v.findViewById(R.id.additional);
                h.Title.setTypeface(Typefacer.rCondensedRegular);
                v.setTag(h);
            } else {
                h = (Holder) v.getTag();
            }
            ActionItem item = getItem(position);
            h.Icon.setImageResource(item.IconResource);
            h.Title.setText(item.Title);
            if (item.Additional != null) {
                h.Additional.setVisibility(View.VISIBLE);
                h.Additional.setText(item.Additional);
            } else {
                h.Additional.setVisibility(View.GONE);
            }
            return v;
        }

        class Holder {
            ImageView Icon;
            TextView Title;
            TextView Additional;
        }

    }

    private static final class ActionItem {
        public int ActionId;
        public String Title;
        public int IconResource;
        public String Additional;

        public ActionItem(int actionId, String title) {
            ActionId = actionId;
            Title = title;
        }

        public ActionItem(int actionId, String title, int iconResource) {
            this(actionId, title);
            IconResource = iconResource;
        }

        public ActionItem(int actionId, String title, int iconResource, String additional) {
            this(actionId, title, iconResource);
            Additional = additional;
        }

    }

    @Override
    public Object getKey() {
        return TAG;
    }
}
