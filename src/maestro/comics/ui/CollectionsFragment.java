package maestro.comics.ui;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import maestro.comics.R;
import maestro.comics.SVGInstance;
import maestro.comics.data.BookManager;
import maestro.comics.dialogs.ConfirmDialog;
import maestro.comics.dialogs.InputDialog;
import maestro.comics.dialogs.OnDialogConfirmListener;
import maestro.comics.model.BookCollection;

import java.util.List;

/**
 * Created by artyom on 8/13/14.
 */
public class CollectionsFragment extends IndicatorFragment implements BookManager.OnBookManagerEventListener, OnDialogConfirmListener {

    public static final String TAG = CollectionsFragment.class.getSimpleName();

    private FragmentAdapter mAdapter;
    private MenuItem mRemoveItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        BookManager.getInstance().attachListener(TAG, this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        BookManager.getInstance().detachListener(TAG);
    }

    @Override
    public PagerAdapter getPagerAdapter() {
        return mAdapter = new FragmentAdapter(getChildFragmentManager());
    }

    @Override
    public void onBookManagerEvent(BookManager.BOOK_MANAGER_EVENT event, Object object) {
        switch (event) {
            case COLLECTION_ADD:
            case COLLECTION_DELETE:
                if (mAdapter != null)
                    mAdapter.update();
                update();
                return;
        }
    }

    @Override
    public void onDialogConfirm(View view, Object object, int id) {
        if (id == ConfirmDialog.DELETE_COLLECTION_ID) {
            BookCollection mCurrentCollection = mAdapter.getCollectionAt(getCurrentPagerPosition());
            if (mCurrentCollection != null) {
                BookManager.getInstance().deleteCollection(mCurrentCollection);
            }
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (mRemoveItem != null && mAdapter != null){
            mRemoveItem.setVisible(mAdapter.getCount() > 0);
        }
    }

    private class FragmentAdapter extends FragmentStatePagerAdapter {

        private BookCollection[] mCollections;

        public FragmentAdapter(FragmentManager fm) {
            super(fm);
            mCollections = BookManager.getInstance().getCollections();
        }

        public void update() {
            mCollections = BookManager.getInstance().getCollections();
            if (mRemoveItem != null) {
                mRemoveItem.setVisible(getCount() > 0);
            }
        }

        @Override
        public Fragment getItem(int i) {
            return BooksFragment.makeInstance(mCollections[i].Id);
        }

        @Override
        public int getCount() {
            return mCollections != null ? mCollections.length : 0;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mCollections[position].Title;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        public BookCollection getCollectionAt(int position) {
            return mCollections[position];
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.main_collection_menu, menu);
        inflater.inflate(R.menu.search_menu, menu);
        menu.findItem(R.id.search).setIcon(SVGInstance.get().getDrawable(R.raw.ic_search, Color.WHITE));
        menu.findItem(R.id.add_collection).setIcon(SVGInstance.get().getDrawable(R.raw.ic_add, Color.WHITE));
        mRemoveItem = menu.findItem(R.id.remove_collection);
        mRemoveItem.setIcon(SVGInstance.get().getDrawable(R.raw.ic_remove, Color.WHITE));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.add_collection) {
            InputDialog dialog = InputDialog.makeInstance(getString(R.string.enter_collection_name));
            dialog.setOnDialogConfirmListener(mAddCollectionConfirmListener);
            dialog.show(getChildFragmentManager(), InputDialog.TAG);
            return true;
        } else if (item.getItemId() == R.id.remove_collection && mAdapter.getCount() > 0) {
            BookCollection mCurrentCollection = mAdapter.getCollectionAt(getCurrentPagerPosition());
            ConfirmDialog dialog = ConfirmDialog.makeInstance(getString(R.string.remove_collection_confirm)
                    + " " + mCurrentCollection.Title + "?", ConfirmDialog.DELETE_COLLECTION_ID);
            dialog.show(getChildFragmentManager(), ConfirmDialog.TAG);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private OnDialogConfirmListener mAddCollectionConfirmListener = new OnDialogConfirmListener() {
        @Override
        public void onDialogConfirm(View view, Object object, int id) {
            BookManager.getInstance().addCollection((String) object);
        }
    };

    @Override
    public void restoreFragments(List<Fragment> fragments) {
        super.restoreFragments(fragments);
        for (Fragment fragment : fragments) {
            if (fragment instanceof InputDialog) {
                ((InputDialog) fragment).setOnDialogConfirmListener(mAddCollectionConfirmListener);
            }
        }
    }

    @Override
    public Object getKey() {
        return TAG;
    }
}
