package maestro.comics.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.*;
import maestro.comics.R;
import maestro.comics.SVGInstance;
import maestro.comics.Typefacer;
import maestro.comics.data.BookManager;
import maestro.comics.data.BookManager.OnBookManagerEventListener;
import maestro.comics.dialogs.InputDialog;
import maestro.comics.dialogs.OnDialogConfirmListener;
import maestro.comics.model.BookCollection;

/**
 * Created by artyom on 5/11/15.
 */
public class CollectionSelectDialog extends DialogFragment implements OnDialogConfirmListener, OnBookManagerEventListener {

    public static final String TAG = CollectionSelectDialog.class.getSimpleName();
    private ListView mList;
    private CollectionAdapter mAdapter;
    private OnCollectionSelectListener mListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.list_select_view, null);
        mList = (ListView) v.findViewById(R.id.list);
        mList.setAdapter(mAdapter = new CollectionAdapter(getActivity()));
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mListener != null) {
                    mListener.onCollectionSelect(mAdapter.getItem(position));
                    dismiss();
                }
            }
        });
        ((TextView) v.findViewById(R.id.title)).setTypeface(Typefacer.rCondensedRegular);
        ImageView btnAdd = (ImageView) v.findViewById(R.id.add_icon);
        SVGInstance.get().applySVG(btnAdd, R.raw.ic_add, Color.WHITE);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputDialog dialog = InputDialog.makeInstance(getString(R.string.enter_collection_name));
                dialog.setOnDialogConfirmListener(CollectionSelectDialog.this);
                dialog.show(getChildFragmentManager(), InputDialog.TAG);
            }
        });
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getParentFragment() instanceof OnCollectionSelectListener) {
            mListener = (OnCollectionSelectListener) getParentFragment();
        } else if (getActivity() instanceof OnCollectionSelectListener) {
            mListener = (OnCollectionSelectListener) getActivity();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        BookManager.getInstance().attachListener(TAG, this);
    }

    @Override
    public void onDetach() {
        BookManager.getInstance().detachListener(TAG);
        super.onDetach();
    }

    @Override
    public void onDialogConfirm(View view, Object object, int id) {
        BookManager.getInstance().addCollection((String) object);
    }

    @Override
    public void onBookManagerEvent(BookManager.BOOK_MANAGER_EVENT event, Object object) {
        switch (event) {
            case COLLECTION_ADD:
            case COLLECTION_DELETE:
                if (mAdapter != null) {
                    mAdapter.update();
                }
                break;
        }
    }

    public interface OnCollectionSelectListener {

        public void onCollectionSelect(BookCollection collection);

    }

    private class CollectionAdapter extends BaseAdapter {

        private Context mContext;
        private LayoutInflater mInflater;
        private BookCollection[] mCollections;

        public CollectionAdapter(Context context) {
            mContext = context;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mCollections = BookManager.getInstance().getCollections();
        }

        public void update() {
            mCollections = BookManager.getInstance().getCollections();
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mCollections != null ? mCollections.length : 0;
        }

        @Override
        public BookCollection getItem(int position) {
            return mCollections[position];
        }

        @Override
        public long getItemId(int position) {
            return mCollections[position].Id;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.single_item_view, null);
                holder = new Holder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (Holder) convertView.getTag();
            }
            holder.Title.setText(getItem(position).Title);
            return convertView;
        }

        class Holder {

            TextView Title;

            public Holder(View view) {
                Title = (TextView) view.findViewById(R.id.title);
                Title.setTypeface(Typefacer.rCondensedRegular);
            }

        }

    }

}
