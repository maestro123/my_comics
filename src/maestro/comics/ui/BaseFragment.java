package maestro.comics.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.View;
import maestro.comics.Main;

import java.util.List;

/**
 * Created by Artyom on 12.08.2014.
 */
public abstract class BaseFragment extends DialogFragment implements Main.OnBackPressedListener {

    public Main mainActivity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof Main) {
            mainActivity = (Main) activity;
            mainActivity.attachOnBackPressedListener(getKey(), this);
        }
    }

    @Override
    public void onDetach() {
        mainActivity.detachOnBackPressedListener(getKey());
        mainActivity = null;
        super.onDetach();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            List<Fragment> fragments = getChildFragmentManager().getFragments();
            if (fragments != null && fragments.size() > 0)
                restoreFragments(fragments);
        }
    }

    public void restoreFragments(List<Fragment> fragments) {
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    protected final void animateView(final View v, final int state) {
//        if (v.getVisibility() != state || v.getAlpha() != 1f) {
            final boolean visible = state == View.VISIBLE;
            v.animate().alpha(visible ? 1f : 0f).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    if (!visible) {
                        v.setVisibility(state);
                    }
                }

                @Override
                public void onAnimationStart(Animator animation) {
                    super.onAnimationStart(animation);
                    if (visible) {
                        v.setVisibility(state);
                    }
                }
            }).start();
//        }
    }

    public abstract Object getKey();

}
