package maestro.comics.ui;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import maestro.comics.*;
import maestro.comics.data.BookManager;
import maestro.comics.data.BooksAdapter;
import maestro.comics.data.ISearchContract;
import maestro.comics.dialogs.ConfirmDialog;
import maestro.comics.dialogs.OnDialogConfirmListener;
import maestro.comics.model.BookCollection;

/**
 * Created by U1 on 22.06.2015.
 */
public class SearchFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Object>,
        TextWatcher, Main.OnBackPressedListener, OnDialogConfirmListener, CollectionSelectDialog.OnCollectionSelectListener {

    public static final String TAG = SearchFragment.class.getSimpleName();

    private static final int SEARCH_LOADER = TAG.hashCode();

    private ImageButton mBackButton;
    private EditText mSearchEditText;
    private ProgressBar mProgress;
    private RecyclerView mList;
    private View mEmptyView;
    private String mSearchText;
    private BooksAdapter mAdapter;
    private BooksSelectionMode mSelectionMode;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setStyle(STYLE_NORMAL, R.style.AppTheme);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public Loader<Object> onCreateLoader(int i, Bundle bundle) {
        mProgress.animate().alpha(1f).start();
        mEmptyView.animate().alpha(0).start();
        return new SearchLoader(getActivity(), mSearchText, getContract());
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object o) {
        if (o instanceof Object[] && ((Object[]) o).length > 0) {
            mAdapter.update((Object[]) o);
            mEmptyView.animate().alpha(0).start();
        } else {
            mEmptyView.animate().alpha(1f).start();
            mAdapter.update(null);
        }
        mProgress.animate().alpha(0).start();
    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.search_fragment_view, null);
        mBackButton = (ImageButton) v.findViewById(R.id.back_icon);
        mSearchEditText = (EditText) v.findViewById(R.id.search_edit_text);
        mList = (RecyclerView) v.findViewById(R.id.list);
        mEmptyView = v.findViewById(R.id.empty_view);
        mProgress = (ProgressBar) v.findViewById(R.id.progress);
        mSearchEditText.addTextChangedListener(this);
        mProgress.setAlpha(0);
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        mSearchEditText.post(new Runnable() {
            @Override
            public void run() {
                mSearchEditText.requestFocus();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(mSearchEditText, 0);
            }
        });

        SVGInstance.get().applySVG(mBackButton, R.raw.ic_back, Color.WHITE);

        return v;
    }

    @Override
    public void onDetach() {
        hideKeyboard();
        super.onDetach();
    }

    private void hideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Utils.prepareEmptyView(this, -1);
        final int columnCount = getResources().getInteger(R.integer.num_of_columns);
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), columnCount);
        mList.setAdapter(mAdapter = new BooksAdapter(getActivity()));
        mList.setLayoutManager(gridLayoutManager);
        gridLayoutManager.setSpanSizeLookup(new BooksAdapter.BooksSpanSizeLookup(mAdapter, columnCount));

        mAdapter.setOnItemActionListener(new BooksAdapter.OnItemActionListener() {
            @Override
            public void OnItemClick(View v, int position) {
                BookFile file = mAdapter.getItem(position);
                ((Main) getActivity()).openBook(file.MFile.getPath());
            }

            @Override
            public boolean onItemLongClick(View vm, int position) {
                ((AppCompatActivity) getActivity()).startSupportActionMode(mSelectionMode = new BooksSelectionMode(SearchFragment.this, mList, mAdapter, position));
                return true;
            }
        });
    }

    private final ISearchContract getContract() {
        if (getParentFragment() instanceof ISearchContract) {
            return (ISearchContract) getParentFragment();
        } else if (getActivity() instanceof ISearchContract) {
            return (ISearchContract) getActivity();
        }
        return null;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        mSearchText = s.toString();
        if (TextUtils.isEmpty(mSearchText)) {
            mAdapter.update(null);
            mEmptyView.animate().alpha(0).start();
            mProgress.animate().alpha(0).start();
        } else {
            search(mSearchText);
        }
    }

    private final void search(String text) {
        mSearchText = text;
        if (getLoaderManager().getLoader(SEARCH_LOADER) == null) {
            getLoaderManager().initLoader(SEARCH_LOADER, null, this);
        } else {
            getLoaderManager().restartLoader(SEARCH_LOADER, null, this);
        }
    }


    @Override
    public boolean onBackPressed() {
        if (mSelectionMode != null && mSelectionMode.finish()) {
            return true;
        }
        return false;
    }

    @Override
    public Object getKey() {
        return TAG;
    }

    public static class SearchLoader extends AsyncTaskLoader<Object> {

        private ISearchContract mContract;
        private String mPattern;

        public SearchLoader(Context context, String pattern, ISearchContract contract) {
            super(context);
            mPattern = pattern;
            mContract = contract;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public Object loadInBackground() {
            Log.e(TAG, "search: " + mPattern + ", contract: " + mContract);
            return mContract != null ? mContract.search(mPattern) : null;
        }
    }


    @Override
    public void onDialogConfirm(View view, Object object, int id) {
        if (id == ConfirmDialog.REMOVE_FROM_COLLECTIONS_ID) {
            new Thread() {
                @Override
                public void run() {
                    super.run();
                    BookManager.getInstance().removeFromCollection(mAdapter.getClonedSelectedObjects());
                }
            }.start();
        }
        if (mSelectionMode != null) {
            mSelectionMode.finish();
        }
    }

    @Override
    public void onCollectionSelect(final BookCollection collection) {
        new Thread() {
            @Override
            public void run() {
                super.run();
                BookManager.getInstance().addToCollection(mAdapter.getClonedSelectedObjects(), collection);
            }
        }.start();
        if (mSelectionMode != null) {
            mSelectionMode.finish();
        }
    }

}
