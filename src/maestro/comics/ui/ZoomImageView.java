package maestro.comics.ui;

import android.content.Context;
import android.graphics.*;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.*;
import android.view.animation.DecelerateInterpolator;
import android.widget.Scroller;
import com.dream.android.mim.IMImageView;
import com.dream.android.mim.ImageLoadObject;
import com.dream.android.mim.RecyclingBitmapDrawable;

/**
 * Created by Artyom on img1/20/2015.
 */
public class ZoomImageView extends View implements IMImageView {

    public static final String TAG = ZoomImageView.class.getSimpleName();

    private final float MIN_SCALE = 0.5f;
    private final float MAX_SCALE = 3f;

    private GestureDetector mGesture;
    private ScaleGestureDetector mScaleGestureDetector;
    private ViewGestureListener mGestureListener = new ViewGestureListener();
    private OnZoomChangeListener mZoomChangeListener;
    private InternalTouchEnsurer mTouchEnsurer;
    private OnOverScrollListener mOverScrollListener;
    private Drawable mDrawable;
    private RectF mTempSrc = new RectF();
    private RectF mTempDst = new RectF();
    private Matrix mDrawMatrix, mMatrix, mOriginMatrix;
    private ScaleType mScaleType = ScaleType.FIT_CENTER;
    private float prevFocusX, prevFocusY;
    private float mTouchSlop;
    private float[] matrixParams = new float[9];
    private float mScaleFactor = 1f;
    private float prevMatchWidth, prevMatchHeight;
    private float matchWidth, matchHeight;
    private float prevViewWidth, prevViewHeight;
    private boolean isBoundsConfigured;

    public interface OnZoomChangeListener {
        public void onZoomChanging(float zoom);

        public void onZoomChanged(float zoom);

        public void onMove(float distanceX, float distanceY);
    }

    public interface OnOverScrollListener {
        public void onOverScroll(float distanceX, float distanceY, ScrollState scrollState);

        public void onFling(ScrollState scrollState, boolean left);

        public void onOverScrollEnd();
    }

    public interface InternalTouchEnsurer {
        public boolean onTouchClick(float x, float y, float bx, float by);
    }

    public enum ScrollState {
        NONE, HORIZONTAL, VERTICAL
    }

    public ZoomImageView(Context context) {
        super(context);
        init();
    }

    public ZoomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    void init() {
        mMatrix = new Matrix();
        mOriginMatrix = new Matrix();
        mGesture = new GestureDetector(getContext(), mGestureListener);
        mGesture.setOnDoubleTapListener(mGestureListener);
        mScaleGestureDetector = new ScaleGestureDetector(getContext(), mGestureListener);
        mTouchSlop = ViewConfiguration.get(getContext()).getScaledTouchSlop();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (mDrawable != null) {
            if (!isBoundsConfigured)
                configureBounds();
            if (mDrawMatrix == null && getPaddingTop() == 0 && getPaddingLeft() == 0) {
                mDrawable.draw(canvas);
            } else {
                int saveCount = canvas.getSaveCount();
                canvas.save();
                canvas.translate(getPaddingLeft(), getPaddingTop());
                if (mDrawMatrix != null) {
                    canvas.concat(mDrawMatrix);
                }
                mDrawable.draw(canvas);
                canvas.restoreToCount(saveCount);
            }
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        setImageDrawable(null);
        Object task = getTag(com.dream.android.mim.R.id.MIM_TASK_TAG);
        if (task != null && task instanceof ImageLoadObject) {
            ((ImageLoadObject) task).cancel();
        }
        super.onDetachedFromWindow();
    }

    private boolean blockRequest;

    @Override
    public void setImageDrawable(Drawable drawable) {
        blockRequest = true;
        Drawable previousDrawable = mDrawable;
        mDrawable = drawable;
        isBoundsConfigured = false;
        notifyDrawable(drawable, true);
        notifyDrawable(previousDrawable, false);
        invalidate();
        blockRequest = false;
    }

    @Override
    public void requestLayout() {
        if (!blockRequest)
            super.requestLayout();
    }

    @Override
    public void unscheduleDrawable(Drawable who) {
        super.unscheduleDrawable(who);
        if (who instanceof RecyclingBitmapDrawable) {
            ((RecyclingBitmapDrawable) who).checkState();
        }
    }

    public Drawable getDrawable() {
        return mDrawable;
    }

    @Override
    public void setImageBitmap(Bitmap bitmap) {
        setImageDrawable(new BitmapDrawable(bitmap));
    }

    @Override
    public void setImageResource(int resource) {
        setImageDrawable(getResources().getDrawable(resource));
    }

    public void setZoom(float zoom) {
        mScaleFactor = Math.min(zoom, MAX_SCALE);
        if (mDrawable != null && isBoundsConfigured) {
            zoom(mScaleFactor, getWidth() / 2, 0);
            invalidate();
        } else {
            prevFocusX = 0;
            prevFocusY = 0;
        }
    }

    public void setZoom(float zoom, float fx, float fy) {
        mScaleFactor = Math.min(zoom, MAX_SCALE);
        if (mDrawable != null && isBoundsConfigured) {
            zoom(mScaleFactor, getWidth() * fx, getHeight() * fy);
            invalidate();
        } else {
            prevFocusX = 0;
            prevFocusY = 0;
        }
    }

    public void setFocus(float distanceX, float distanceY) {
        float fixTransX = getFixDragTrans(-distanceX, getWidth(), getDrawableWidth());
        float fixTransY = getFixDragTrans(-distanceY, getHeight(), getDrawableHeight());
        mDrawMatrix.postTranslate(fixTransX, fixTransY);
        fixTrans();
        invalidate();
    }

    public float getZoom() {
        return mScaleFactor;
    }

    public void setOnZoomChangeListener(OnZoomChangeListener listener) {
        mZoomChangeListener = listener;
    }

    public void setInternalTouchEnsurer(InternalTouchEnsurer ensurer) {
        mTouchEnsurer = ensurer;
    }

    public void setOnOverScrollListener(OnOverScrollListener listener) {
        mOverScrollListener = listener;
    }

    public void configureBounds() {
        if (mDrawable != null) {
            Matrix prevMatrix = new Matrix(mDrawMatrix);

            int dwidth = mDrawable.getIntrinsicWidth();
            int dheight = mDrawable.getIntrinsicHeight();

            int vwidth = getWidth() - getPaddingLeft() - getPaddingRight();
            int vheight = getHeight() - getPaddingTop() - getPaddingBottom();

            boolean fits = (dwidth < 0 || vwidth == dwidth) && (dheight < 0 || vheight == dheight);

            if (dwidth <= 0 || dheight <= 0 || ScaleType.FIT_XY == mScaleType) {
                mDrawable.setBounds(0, 0, vwidth, vheight);
                mDrawMatrix = new Matrix();
            } else {
                mDrawable.setBounds(0, 0, dwidth, dheight);

                if (ScaleType.MATRIX == mScaleType) {
                    if (mMatrix.isIdentity()) {
                        mDrawMatrix = null;
                    } else {
                        mDrawMatrix = mMatrix;
                    }
                } else if (fits) {
                    mDrawMatrix = new Matrix();
                } else if (ScaleType.CENTER == mScaleType) {
                    mDrawMatrix = mMatrix;
                    mDrawMatrix.setTranslate((int) ((vwidth - dwidth) * 0.5f + 0.5f),
                            (int) ((vheight - dheight) * 0.5f + 0.5f));
                } else if (ScaleType.CENTER_CROP == mScaleType) {
                    mDrawMatrix = mMatrix;

                    float scale;
                    float dx = 0, dy = 0;

                    if (dwidth * vheight > vwidth * dheight) {
                        scale = (float) vheight / (float) dheight;
                        dx = (vwidth - dwidth * scale) * 0.5f;
                    } else {
                        scale = (float) vwidth / (float) dwidth;
                        dy = (vheight - dheight * scale) * 0.5f;
                    }

                    mDrawMatrix.setScale(scale, scale);
                    mDrawMatrix.postTranslate((int) (dx + 0.5f), (int) (dy + 0.5f));
                } else if (ScaleType.CENTER_INSIDE == mScaleType) {
                    mDrawMatrix = mMatrix;
                    float scale;
                    float dx;
                    float dy;

                    if (dwidth <= vwidth && dheight <= vheight) {
                        scale = 1.0f;
                    } else {
                        scale = Math.min((float) vwidth / (float) dwidth,
                                (float) vheight / (float) dheight);
                    }

                    dx = (int) ((vwidth - dwidth * scale) * 0.5f + 0.5f);
                    dy = (int) ((vheight - dheight * scale) * 0.5f + 0.5f);

                    mDrawMatrix.setScale(scale, scale);
                    mDrawMatrix.postTranslate(dx, dy);
                } else {
                    mTempSrc.set(0, 0, dwidth, dheight);
                    mTempDst.set(0, 0, vwidth, vheight);

                    mDrawMatrix = mMatrix;
                    mDrawMatrix.setRectToRect(mTempSrc, mTempDst, scaleTypeToScaleToFit(mScaleType));
                }

                if (mDrawMatrix != null) {
                    mDrawMatrix.getValues(matrixParams);
                }
                float redundantYSpace = vheight - (matrixParams[Matrix.MSCALE_Y] * dheight);
                float redundantXSpace = vwidth - (matrixParams[Matrix.MSCALE_X] * dwidth);
                matchWidth = vwidth - redundantXSpace;
                matchHeight = vheight - redundantYSpace;

                mOriginMatrix.set(mDrawMatrix);

                if (mScaleFactor != 1f) {
                    if (prevFocusX == 0 && matchWidth < vwidth)
                        prevFocusX = vwidth / 2;

                    zoom(mScaleFactor, prevFocusX, prevFocusY);

                    if (prevMatchHeight > 0 && prevMatchWidth > 0 && prevFocusX > 0 && prevFocusY > 0) {
                        prevMatrix.getValues(matrixParams);
                        matrixParams[Matrix.MSCALE_X] = matchWidth / dwidth * mScaleFactor;
                        matrixParams[Matrix.MSCALE_Y] = matchHeight / dheight * mScaleFactor;

                        float transX = matrixParams[Matrix.MTRANS_X];
                        float transY = matrixParams[Matrix.MTRANS_Y];

                        float prevActualWidth = prevMatchWidth * mScaleFactor;
                        float actualWidth = getDrawableWidth();
                        translateMatrixAfterRotate(Matrix.MTRANS_X, transX, prevActualWidth, actualWidth, prevViewWidth, vwidth, dwidth);

                        float prevActualHeight = prevMatchHeight * mScaleFactor;
                        float actualHeight = getDrawableHeight();
                        translateMatrixAfterRotate(Matrix.MTRANS_Y, transY, prevActualHeight, actualHeight, prevViewHeight, vheight, dheight);

                        mDrawMatrix.setValues(matrixParams);

                    }

                }

                prevMatchWidth = matchWidth;
                prevMatchHeight = matchHeight;
                prevViewWidth = vwidth;
                prevViewHeight = vheight;
            }
            isBoundsConfigured = true;
        }
    }

    public void zoom(float scale, float fx, float fy) {
        Log.e(TAG, "scale: " + scale + ", coords: " + fx + "/" + fy);
        mDrawMatrix.set(mOriginMatrix);
        mDrawMatrix.postScale(scale, scale, fx, fy);
        prevFocusX = fx;
        prevFocusY = fy;
        fixScaleTrans();
    }

    public void move(float fX, float fY) {
        if (mDrawMatrix != null) {
            setFocus(fX, fY);
        }
        prevFocusX = fX;
        prevFocusY = fY;
    }

    private PointF transformCoordBitmapToTouch(float bx, float by) {
        if (getDrawable() == null) return new PointF(bx, by);
        mDrawMatrix.getValues(matrixParams);
        float origW = getDrawable().getIntrinsicWidth();
        float origH = getDrawable().getIntrinsicHeight();
        float px = bx / origW;
        float py = by / origH;
        float finalX = matrixParams[Matrix.MTRANS_X] + getDrawableWidth() * px;
        float finalY = matrixParams[Matrix.MTRANS_Y] + getDrawableHeight() * py;
        return new PointF(finalX, finalY);
    }

    private PointF transformCoordTouchToBitmap(float x, float y, boolean clipToBitmap) {
        if (getDrawable() == null) return new PointF(x, y);
        mDrawMatrix.getValues(matrixParams);
        float origW = getDrawable().getIntrinsicWidth();
        float origH = getDrawable().getIntrinsicHeight();
        float transX = matrixParams[Matrix.MTRANS_X];
        float transY = matrixParams[Matrix.MTRANS_Y];
        float finalX = ((x - transX) * origW) / getDrawableWidth();
        float finalY = ((y - transY) * origH) / getDrawableHeight();

        if (clipToBitmap) {
            finalX = Math.min(Math.max(x, 0), origW);
            finalY = Math.min(Math.max(y, 0), origH);
        }

        return new PointF(finalX, finalY);
    }

    private void translateMatrixAfterRotate(int axis, float trans, float prevImageSize, float imageSize, float prevViewSize, float viewSize, float drawableSize) {
        if (imageSize < viewSize) {
            matrixParams[axis] = (viewSize - (drawableSize * matrixParams[Matrix.MSCALE_X])) * 0.5f;
        } else if (trans > 0) {
            matrixParams[axis] = -((imageSize - viewSize) * 0.5f);
        } else {
            float percentage = (Math.abs(trans) + (0.5f * prevViewSize)) / prevImageSize;
            matrixParams[axis] = -((percentage * imageSize) - (viewSize * 0.5f));
        }
    }

    private float mStartX, mStartY;
    private float mPrevX, mPrevY;
    private float absX, absY;
    private ScrollState mScrollState;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!isBoundsConfigured) {
            return super.onTouchEvent(event);
        }
        if (event.getPointerCount() > 1) {
            mScaleGestureDetector.onTouchEvent(event);
        } else {
            mGesture.onTouchEvent(event);
        }
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mPrevX = mStartX = event.getRawX();
                mPrevY = mStartY = event.getRawY();
                break;
            case MotionEvent.ACTION_MOVE:
                absX = event.getRawX() - mStartX;
                absY = event.getRawY() - mStartY;
                mPrevX = event.getRawX();
                mPrevY = event.getRawY();
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                mGestureListener.onRelease();
                if (mOverScrollListener != null)
                    mOverScrollListener.onOverScrollEnd();
                mScrollState = ScrollState.NONE;
                break;
        }
        return true;
    }

    private static Matrix.ScaleToFit scaleTypeToScaleToFit(ScaleType st) {
        return sS2FArray[st.nativeInt - 1];
    }

    private class ViewGestureListener extends GestureDetector.SimpleOnGestureListener implements ScaleGestureDetector.OnScaleGestureListener {

        private boolean onScale;
        private boolean onFling;
        private boolean isInitial = true;

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            compatPostOnAnimation(new AnimateZoomRunnable(mScaleFactor, mScaleFactor > 1f ? 1f : MAX_SCALE / 2, e.getRawX(), e.getRawY()));
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            mDrawMatrix.getValues(matrixParams);
            if (!onScale) {
                if (mScaleFactor != 1f) {
                    if (isInitial) {
                        isInitial = false;
                        return true;
                    }
                    setFocus(distanceX, distanceY);
                    if (mZoomChangeListener != null)
                        mZoomChangeListener.onMove(distanceX, distanceY);
                    return true;
                } else if (mOverScrollListener != null && !onFling) {
                    if (mScrollState == ScrollState.NONE) {
                        if (Math.abs(absX) > mTouchSlop)
                            mScrollState = ScrollState.HORIZONTAL;
                        else if (Math.abs(absY) > mTouchSlop) {
                            mScrollState = ScrollState.VERTICAL;
                        }
                    }
                    absX -= mTouchSlop;
                    absY -= mTouchSlop;
                    mOverScrollListener.onOverScroll(absX, absY, mScrollState);
                }
            }
            return false;
        }

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scale(detector.getScaleFactor(), detector.getFocusX(), detector.getFocusY());
            if (mZoomChangeListener != null)
                mZoomChangeListener.onZoomChanging(mScaleFactor);
            return true;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            return onScale = true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            if (mScaleFactor < 1f) {
                compatPostOnAnimation(new AnimateZoomRunnable(mScaleFactor, 1f, getWidth() / 2, getHeight() / 2));
            }
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            onFling = true;
            if (mScaleFactor == 1f && mOverScrollListener != null) {
                mOverScrollListener.onFling(mScrollState, mScrollState == ScrollState.VERTICAL ? absY > 0 : absX > 0);
            } else
                compatPostOnAnimation(new FlingRunnable((int) velocityX, (int) velocityY));
            return super.onFling(e1, e2, velocityX, velocityY);
        }

        public void onRelease() {
            if (onScale && mZoomChangeListener != null)
                mZoomChangeListener.onZoomChanged(mScaleFactor);
            onScale = false;
            onFling = false;
            isInitial = true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            PointF bCoord = transformCoordTouchToBitmap(e.getX(), e.getY(), false);
            return (mTouchEnsurer != null ? mTouchEnsurer.onTouchClick(e.getX(), e.getY(), bCoord.x, bCoord.y) : false) || (!onScale && !onFling && performClick());
        }
    }

    private void scale(float factor, float focusX, float focusY) {
        Log.e(TAG, "focusX = " + focusX + ", focusY = " + focusY);
        float origScale = mScaleFactor;
        mScaleFactor *= factor;
        if (mScaleFactor > MAX_SCALE) {
            mScaleFactor = MAX_SCALE;
            factor = MAX_SCALE / origScale;
        } else if (mScaleFactor < MIN_SCALE) {
            mScaleFactor = MIN_SCALE;
            factor = MIN_SCALE / origScale;
        }
        if (mScaleFactor != origScale) {
            mDrawMatrix.postScale(factor, factor, focusX, focusY);
            fixScaleTrans();
            invalidate();
            prevFocusX = focusX;
            prevFocusY = focusY;
        }
    }

    private void fixScaleTrans() {
        fixTrans();
        mDrawMatrix.getValues(matrixParams);
        if (getDrawableWidth() < getWidth()) {
            matrixParams[Matrix.MTRANS_X] = (getWidth() - getDrawableWidth()) / 2;
        }

        if (getDrawableHeight() < getHeight()) {
            matrixParams[Matrix.MTRANS_Y] = (getHeight() - getDrawableHeight()) / 2;
        }
        mDrawMatrix.setValues(matrixParams);
    }

    private void fixTrans() {
        mDrawMatrix.getValues(matrixParams);
        float transX = matrixParams[Matrix.MTRANS_X];
        float transY = matrixParams[Matrix.MTRANS_Y];

        RectF rect = new RectF();
        mDrawMatrix.mapRect(rect);

        float fixTransX = getFixTrans(transX, getWidth(), getDrawableWidth());
        float fixTransY = getFixTrans(transY, getHeight(), getDrawableHeight());

        if (fixTransX != 0 || fixTransY != 0) {
            mDrawMatrix.postTranslate(fixTransX, fixTransY);
        }
    }

    private float getFixTrans(float trans, float viewSize, float contentSize) {
        float minTrans, maxTrans;

        if (contentSize <= viewSize) {
            minTrans = 0;
            maxTrans = viewSize - contentSize;

        } else {
            minTrans = viewSize - contentSize;
            maxTrans = 0;
        }

        if (trans < minTrans)
            return -trans + minTrans;
        if (trans > maxTrans)
            return -trans + maxTrans;
        return 0;
    }

    private float getFixDragTrans(float delta, float viewSize, float contentSize) {
        if (contentSize <= viewSize) {
            return 0;
        }
        return delta;
    }

    public float getDrawableWidth() {
        return matchWidth * mScaleFactor;
    }

    public float getDrawableHeight() {
        return matchHeight * mScaleFactor;
    }

    private void compatPostOnAnimation(Runnable runnable) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            postOnAnimation(runnable);
        } else {
            postDelayed(runnable, 1000 / 60);
        }
    }

    private class AnimateZoomRunnable implements Runnable {

        final float TIME = 300;

        final float fromScale, toScale;
        final float toX, toY;
        final long startTime;
        final DecelerateInterpolator interpolator = new DecelerateInterpolator(1.2f);

        public AnimateZoomRunnable(float fromScale, float toScale, float toX, float toY) {
            this.fromScale = fromScale;
            this.toScale = toScale;
            this.toX = toX;
            this.toY = toY;
            startTime = System.currentTimeMillis();
        }

        @Override
        public void run() {
            float t = interpolate();
            float deltaScale = calculateDeltaScale(t);

            scale(deltaScale, toX, toY);

            if (t < 1f) {
                if (mZoomChangeListener != null) {
                    mZoomChangeListener.onZoomChanging(mScaleFactor);
                }
                compatPostOnAnimation(this);
            } else {
                if (mZoomChangeListener != null)
                    mZoomChangeListener.onZoomChanged(mScaleFactor);
            }

        }

        private float interpolate() {
            long currTime = System.currentTimeMillis();
            float elapsed = (currTime - startTime) / TIME;
            elapsed = Math.min(1f, elapsed);
            return interpolator.getInterpolation(elapsed);
        }

        private float calculateDeltaScale(float t) {
            float scale = fromScale + t * (toScale - fromScale);
            return scale / mScaleFactor;
        }

    }

    private class FlingRunnable implements Runnable {

        Scroller scroller;
        int currX, currY;

        FlingRunnable(int velocityX, int velocityY) {
            scroller = new Scroller(getContext());
            mDrawMatrix.getValues(matrixParams);

            int startX = (int) matrixParams[Matrix.MTRANS_X];
            int startY = (int) matrixParams[Matrix.MTRANS_Y];
            int minX, maxX, minY, maxY;

            if (getDrawableWidth() > getWidth()) {
                minX = getWidth() - (int) getDrawableWidth();
                maxX = 0;
            } else {
                minX = maxX = startX;
            }

            if (getDrawableHeight() > getHeight()) {
                minY = getHeight() - (int) getDrawableHeight();
                maxY = 0;

            } else {
                minY = maxY = startY;
            }

            float offset = startX > startY ? startX - maxX : startY - maxY;
            float initialVelocity = startX > startY ? velocityX : velocityY;

            scroller.extendDuration((int) (Math.abs(offset) / Math.max(1000, Math.abs(initialVelocity))));
            scroller.fling(startX, startY, velocityX / 2, velocityY / 2, minX,
                    maxX, minY, maxY);
            currX = startX;
            currY = startY;
        }

        public void cancelFling() {
            if (scroller != null) {
                scroller.forceFinished(true);
            }
        }

        @Override
        public void run() {
            if (scroller.isFinished()) {
                scroller = null;
                return;
            }

            if (scroller.computeScrollOffset()) {
                int newX = scroller.getCurrX();
                int newY = scroller.getCurrY();
                int transX = newX - currX;
                int transY = newY - currY;
                currX = newX;
                currY = newY;
                mDrawMatrix.postTranslate(transX, transY);
                fixTrans();
                invalidate();
                compatPostOnAnimation(this);
            }
        }
    }

    @Override
    public boolean canScrollHorizontally(int direction) {
        if (mDrawMatrix != null) {
            mDrawMatrix.getValues(matrixParams);
            return !(matrixParams[Matrix.MTRANS_X] > 0 || (direction < 0 ? matrixParams[Matrix.MTRANS_X] == 0 : Math.abs(matrixParams[Matrix.MTRANS_X]) >= Math.abs(getWidth() - getDrawableWidth())));
        }
        return super.canScrollHorizontally(direction);
    }

    @Override
    public boolean canScrollVertically(int direction) {
        if (mDrawMatrix != null) {
            mDrawMatrix.getValues(matrixParams);
            return !(matrixParams[Matrix.MTRANS_Y] > 0 || (direction < 0 ? matrixParams[Matrix.MTRANS_Y] == 0 : Math.abs(matrixParams[Matrix.MTRANS_Y]) == Math.abs(getHeight() - getDrawableHeight())));
        }
        return super.canScrollVertically(direction);
    }

    private static final Matrix.ScaleToFit[] sS2FArray = {
            Matrix.ScaleToFit.FILL,
            Matrix.ScaleToFit.START,
            Matrix.ScaleToFit.CENTER,
            Matrix.ScaleToFit.END
    };

    public enum ScaleType {
        MATRIX(0),

        FIT_XY(1),

        FIT_START(2),

        FIT_CENTER(3),

        FIT_END(4),

        CENTER(5),

        CENTER_CROP(6),

        CENTER_INSIDE(7);

        ScaleType(int ni) {
            nativeInt = ni;
        }

        final int nativeInt;
    }

    protected static void notifyDrawable(Drawable drawable, final boolean isDisplayed) {
        if (drawable instanceof RecyclingBitmapDrawable) {
            ((RecyclingBitmapDrawable) drawable).setIsDisplayed(isDisplayed);
        } else if (drawable instanceof LayerDrawable) {
            LayerDrawable layerDrawable = (LayerDrawable) drawable;
            for (int i = 0, z = layerDrawable.getNumberOfLayers(); i < z; i++) {
                notifyDrawable(layerDrawable.getDrawable(i), isDisplayed);
            }
        }
    }

}
