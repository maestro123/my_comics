package maestro.comics;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import maestro.comics.data.BookManager;
import maestro.comics.data.ISearchContract;
import maestro.comics.read.ReadActivity;
import maestro.comics.ui.BooksFragment;
import maestro.comics.ui.CollectionsFragment;
import maestro.comics.ui.NavigationFragment;
import maestro.comics.ui.SearchFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;


public class Main extends AppCompatActivity implements BookManager.OnBookManagerEventListener, ISearchContract {

    public static final String TAG = Main.class.getSimpleName();

    private static final String SAVED_PREVIOUS_ID = "previous_id";

    private int mCurrentFragmentId = -1;

    private DrawerLayout mDrawer;
    private Toolbar mToolbar;
    private ActionBarDrawerToggle mDrawerToggle;
    private FragmentTransaction mTransaction;

    private HashMap<Object, OnBackPressedListener> mBackPressedListeners = new HashMap<Object, OnBackPressedListener>();

    @Override
    public Object search(String pattern) {
        BookFile[] mFiles = BookManager.getInstance().getBooks(BookManager.NO_COLLECTION);
        if (mFiles != null && mFiles.length > 0) {
            ArrayList<BookFile> primaryFiles = new ArrayList<>();
            ArrayList<BookFile> secondaryFiles = new ArrayList<>();
            for (BookFile file : mFiles) {
                if (file.MFile.getName().toLowerCase().startsWith(pattern.toLowerCase())) {
                    primaryFiles.add(file);
                } else if (file.MFile.getName().toLowerCase().contains(pattern.toLowerCase())) {
                    secondaryFiles.add(file);
                }
            }
            Collections.sort(primaryFiles, new BookManager.NameComparator(false));
            return primaryFiles.size() > 0 ? primaryFiles.toArray() : secondaryFiles.toArray();
        }
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        setSupportActionBar(mToolbar);

        for (int i = 0; i < mToolbar.getChildCount(); i++) {
            if (mToolbar.getChildAt(i) instanceof TextView) {
                TextView mTextView = (TextView) mToolbar.getChildAt(i);
                mTextView.setTextColor(Color.WHITE);
                mTextView.setTypeface(Typefacer.rCondensedRegular);
            }
        }

        BookManager.getInstance().attachListener(TAG, this);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.navigation_frame, new NavigationFragment()).commit();
            openFragmentById(NavigationFragment.ACTION_MAIN);
        } else {
            mCurrentFragmentId = savedInstanceState.getInt(SAVED_PREVIOUS_ID);
        }
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                commitTransaction();
            }
        };
        mDrawer.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        if (!ensureBackPress())
            super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        BookManager.getInstance().detachListener(TAG);
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVED_PREVIOUS_ID, mCurrentFragmentId);
    }

    @Override
    public void onBookManagerEvent(BookManager.BOOK_MANAGER_EVENT event, Object object) {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        MenuItem item = menu.findItem(R.id.search);
        item.setIcon(SVGInstance.get().getDrawable(R.raw.ic_search, Color.WHITE));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        } else if (R.id.search == item.getItemId()) {
            getSupportFragmentManager().beginTransaction().replace(R.id.search_frame, new SearchFragment(), SearchFragment.TAG)
                    .setCustomAnimations(R.anim.frg_in, R.anim.frg_out).addToBackStack(SearchFragment.TAG).commit();
//            new SearchFragment().show(getSupportFragmentManager(), SearchFragment.TAG);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }

    public void openBook(String path) {
        Intent i = new Intent(this, ReadActivity.class);
        i.putExtra(ReadActivity.PARAM_PATH, path);
        startActivityForResult(i, 0);
    }

    public void openFragmentById(int id) {
        if (id != mCurrentFragmentId) {
            mTransaction = getSupportFragmentManager().beginTransaction();
            switch (id) {
                case NavigationFragment.ACTION_MAIN:
                    mTransaction.replace(R.id.main_frame, BooksFragment.makeInstance(BookManager.NO_COLLECTION));
                    break;
                case NavigationFragment.ACTION_COLLECTIONS:
                    mTransaction.replace(R.id.main_frame, new CollectionsFragment());
                    break;
                case NavigationFragment.ACTION_FILES:
                    mTransaction.replace(R.id.main_frame, new FileManagerFragment());
                    break;
                case NavigationFragment.ACTION_SETTINGS:

                    break;
            }
            mTransaction.setCustomAnimations(R.anim.frg_in, R.anim.frg_out);
            if (mDrawer == null || !mDrawer.isDrawerOpen(Gravity.LEFT))
                commitTransaction();
        }
        if (mDrawer != null)
            mDrawer.closeDrawer(Gravity.LEFT);
    }

    private void commitTransaction() {
        if (mTransaction != null) {
            mTransaction.commit();
            mTransaction = null;
        }
    }

    public void attachOnBackPressedListener(Object key, OnBackPressedListener listener) {
        if (mBackPressedListeners.containsKey(key))
            mBackPressedListeners.remove(key);
        mBackPressedListeners.put(key, listener);
    }

    public void setDrawerState(boolean available) {
        if (mDrawer != null) {
            mDrawer.setDrawerLockMode(available ? DrawerLayout.LOCK_MODE_UNLOCKED : DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }

    public void detachOnBackPressedListener(Object key) {
        mBackPressedListeners.remove(key);
    }

    private boolean ensureBackPress() {
        ArrayList<OnBackPressedListener> listeners = new ArrayList<OnBackPressedListener>(mBackPressedListeners.values());
        for (int i = listeners.size() - 1; i > -1; i--) {
            if (listeners.get(i).onBackPressed()) {
                return true;
            }
        }
        return false;
    }

    public interface OnBackPressedListener {
        public boolean onBackPressed();
    }

}
