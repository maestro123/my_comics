package maestro.comics.data;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.dream.android.mim.MIM;
import com.dream.android.mim.RecyclingImageView;
import maestro.comics.*;
import maestro.comics.ui.BooksSelectionMode;
import maestro.support.v1.svg.SVGHelper;

import java.util.ArrayList;

/**
 * Created by U1 on 22.06.2015.
 */
public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.Holder>
        implements View.OnClickListener, View.OnLongClickListener {

    public static final int TYPE_DIVIDER = 0x01;
    public static final int TYPE_NORMAL = 0x02;

    private final Object mSyncObject = new Object();

    private Object[] mItems;
    private ArrayList<Object> mSelectedObjects = new ArrayList<>();
    private Context mContext;
    private LayoutInflater mInflater;
    private BooksSelectionMode mSelectionMode;
    private OnItemActionListener mItemActionListener;

    private int parentWidth;
    private int parentHeight;
    private int imageWidth;
    private int imageHeight;
    private int topPadding;
    private int numOfColumns;

    public BooksAdapter(Context context) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final Resources resources = context.getResources();
        final DisplayMetrics metrics = resources.getDisplayMetrics();
        numOfColumns = resources.getInteger(R.integer.num_of_columns);
        parentWidth = metrics.widthPixels / numOfColumns;
        imageWidth = parentWidth - (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, metrics);
        imageHeight = Math.round(imageWidth * 1.5f);
        parentHeight = imageHeight + (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60, metrics);
        topPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, metrics);
    }

    public void setOnItemActionListener(OnItemActionListener listener) {
        mItemActionListener = listener;
    }

    public void startSelection(BooksSelectionMode mode, RecyclerView list, int position) {
        mSelectedObjects.clear();
        mSelectionMode = mode;
        mSelectedObjects.add(getItem(position));
        for (int i = 0; i < list.getChildCount(); i++) {
            Holder holder = (Holder) list.getChildAt(i).getTag();
            if (holder != null) {
                holder.SelectionMask.setVisibility(mSelectedObjects.contains(getItem(holder.getAdapterPosition()))
                        ? View.VISIBLE : View.GONE);
            }
        }
        mSelectionMode.setSelectedCount(getSelectedCount());
    }

    public void finishSelection(RecyclerView list) {
        mSelectionMode = null;
        for (int i = 0; i < list.getChildCount(); i++) {
            Holder holder = (Holder) list.getChildAt(i).getTag();
            if (holder != null) {
                holder.SelectionMask.setVisibility(View.GONE);
            }
        }
    }

    public void setAllSelected(RecyclerView list, boolean selected) {
        synchronized (mSyncObject) {
            if (selected) {
                for (int i = 0; i < getItemCount(); i++) {
                    if (mItems[i] instanceof BookFile && !mSelectedObjects.contains(mItems[i])) {
                        mSelectedObjects.add(mItems[i]);
                    }
                }
            } else {
                mSelectedObjects.clear();
            }
            for (int i = 0; i < list.getChildCount(); i++) {
                Holder holder = (Holder) list.getChildAt(i).getTag();
                if (holder != null) {
                    holder.SelectionMask.setVisibility(selected ? View.VISIBLE : View.GONE);
                }
            }
            mSelectionMode.setSelectedCount(getSelectedCount());
        }
    }

    public void update(Object[] files) {
        synchronized (mSyncObject) {
            mItems = files;
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return mItems != null ? mItems.length : 0;
    }

    public BookFile getItem(int i) {
        return (BookFile) mItems[i];
    }

    public boolean haveSelectedItems(){
        return mSelectionMode != null && mSelectedObjects.size() > 0;
    }

    public int getSelectedCount() {
        return mSelectedObjects.size();
    }

    public ArrayList<Object> getClonedSelectedObjects() {
        return (ArrayList<Object>) mSelectedObjects.clone();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemViewType(int position) {
        return mItems[position] instanceof BookFile ? TYPE_NORMAL : TYPE_DIVIDER;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = mInflater.inflate(i == TYPE_DIVIDER ? R.layout.row_divider : R.layout.book_item_view, null);
        Holder holder = new Holder(v, i);
        if (i == TYPE_NORMAL) {
            v.setTag(holder);
            v.setOnClickListener(this);
            v.setOnLongClickListener(this);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(Holder h, int position) {
        if (getItemViewType(position) == TYPE_NORMAL) {
            h.itemView.setPadding(h.itemView.getPaddingLeft(), position < numOfColumns ? topPadding : 0, h.itemView.getPaddingRight(), h.itemView.getPaddingBottom());
            BookFile file = (BookFile) mItems[position];
            h.Title.setText(file.MFile.getName());
            h.Image.setLoadObject(MIM.by(Utils.MIM_COMICS).to(h.Image, file.MFile.getPath()).object(file));
            if (file.ReadProgress != null) {
                h.Additional.setVisibility(View.VISIBLE);
                h.Additional.setText(Utils.makeReadProgress(file.ReadProgress, null));
            } else {
                h.Additional.setVisibility(View.GONE);
            }
            h.SelectionMask.setVisibility(mSelectionMode != null
                    && mSelectedObjects.contains(file) ? View.VISIBLE : View.GONE);
        } else {
            h.Title.setText((Integer) mItems[position]);
        }
    }

    @Override
    public void onClick(View v) {
        Holder holder = (Holder) v.getTag();
        if (mSelectionMode != null) {
            Object object = getItem(holder.getAdapterPosition());
            if (mSelectedObjects.contains(object)) {
                mSelectedObjects.remove(object);
                holder.SelectionMask.setVisibility(View.GONE);
            } else {
                mSelectedObjects.add(object);
                holder.SelectionMask.setVisibility(View.VISIBLE);
            }
            mSelectionMode.setSelectedCount(mSelectedObjects.size());
        } else if (mItemActionListener != null) {
            mItemActionListener.OnItemClick(v, holder.getPosition());
        }
    }

    @Override
    public boolean onLongClick(View v) {
        Holder holder = (Holder) v.getTag();
        return mSelectionMode == null && mItemActionListener != null ? mItemActionListener.onItemLongClick(v, holder.getPosition()) : false;
    }

    public interface OnItemActionListener {
        public void OnItemClick(View v, int position);

        public boolean onItemLongClick(View vm, int position);
    }

    public static class BooksSpanSizeLookup extends GridLayoutManager.SpanSizeLookup {

        private BooksAdapter mAdapter;
        private int mColumnCount;

        public BooksSpanSizeLookup(BooksAdapter adapter, int columnCount) {
            mAdapter = adapter;
            mColumnCount = columnCount;
        }

        @Override
        public int getSpanSize(int i) {
            return mAdapter.getItemViewType(i) == BooksAdapter.TYPE_DIVIDER ? mColumnCount : 1;
        }

    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView Title;
        TextView Additional;
        RecyclingImageView Image;
        View SelectionMask;
        ImageView SelectionCheck;

        public Holder(View v, int type) {
            super(v);
            Title = (TextView) v.findViewById(R.id.title);
            Title.setTypeface(Typefacer.rCondensedRegular);
            if (type == TYPE_NORMAL) {
                Image = (RecyclingImageView) v.findViewById(R.id.image);
                Additional = (TextView) v.findViewById(R.id.additional);
                SelectionMask = v.findViewById(R.id.selection_mask);
                SelectionCheck = (ImageView) v.findViewById(R.id.selection_check_icon);
                Image.setHasFixedSize(true);
                Image.getLayoutParams().width = imageWidth;
                Image.getLayoutParams().height = imageHeight;
                v.findViewById(R.id.file_item_view).getLayoutParams().width = parentWidth;
                v.findViewById(R.id.file_item_view).getLayoutParams().height = parentHeight;
                SVGInstance.get().applySVG(SelectionCheck, R.raw.ic_done, Color.WHITE, SVGHelper.DPI + 1.5f);
            }
        }
    }

}
