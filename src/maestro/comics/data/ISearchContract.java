package maestro.comics.data;

/**
 * Created by U1 on 29.06.2015.
 */
public interface ISearchContract {
    public Object search(String pattern);
}
