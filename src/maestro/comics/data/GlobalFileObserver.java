package maestro.comics.data;

import android.os.FileObserver;
import android.util.Log;
import maestro.comics.Utils;

import java.io.File;

/**
 * Created by Artyom on 12.08.2014.
 */
public class GlobalFileObserver extends FileObserver {

    public static final String TAG = GlobalFileObserver.class.getSimpleName();
    private final String ROOT_PATH;

    public GlobalFileObserver(String path) {
        super(path);
        ROOT_PATH = path.endsWith("/") ? path : path + "/";
    }

    @Override
    public void onEvent(int i, String s) {
        if (s != null) {
            try {
                File f = new File(ROOT_PATH + s);
                final boolean isOurFile = f.getName()
                        .toLowerCase().matches(Utils.IMAGE_PATTERN);
                if (isOurFile) {
                    Log.e(TAG, "find our file on path = " + f.getPath());
                }
//                Log.e(TAG, toReadableEvent(i));
                if (isOurFile) {
                    switch (i) {
                        case DELETE:

                            break;
                        case CREATE:

                            break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getRootPath() {
        return ROOT_PATH;
    }

    private final String toReadableEvent(int i) {
        switch (i) {
            case CREATE:
                return "CREATE";
            case DELETE:
                return "DELETE";
            case ACCESS:
                return "ACCESS";
            case OPEN:
                return "OPEN";
            case MOVED_TO:
                return "MOVED_TO";
            case ATTRIB:
                return "ATTRIB";
        }
        return "i ( " + i + " ) not parametrize";
    }

}
