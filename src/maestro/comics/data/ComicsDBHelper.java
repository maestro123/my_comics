package maestro.comics.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import maestro.comics.BookFile;
import maestro.comics.MJavaFile;
import maestro.comics.Utils;
import maestro.comics.model.BookCollection;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by artyom on 8/11/14.
 */
public class ComicsDBHelper {

    public static final String TAG = ComicsDBHelper.class.getSimpleName();

    private static volatile ComicsDBHelper instance;
    private Context mContext;
    private SQLiteDatabase mDatabase;
    private ComicsDB mDB;

    public static synchronized ComicsDBHelper getInstance() {
        ComicsDBHelper localInstance = instance;
        if (localInstance == null) {
            synchronized (ComicsDBHelper.class) {
                localInstance = instance;
                if (localInstance == null) {
                    localInstance = instance = new ComicsDBHelper();
                }
            }
        }
        return localInstance;
    }

    private static BookFile convertToBookFile(Cursor cursor) {
        File f = new File(Utils.decodePath(cursor.getString(cursor.getColumnIndexOrThrow(ComicsDB.COLUMN_PATH))));
        if (f.exists()) {
            return new BookFile(cursor.getLong(cursor.getColumnIndexOrThrow(ComicsDB.COLUMN_ID)),
                    new MJavaFile(f, f.getParentFile()),
                    Integer.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(ComicsDB.COLUMN_SAVED_POSITION))),
                    cursor.getString(cursor.getColumnIndexOrThrow(ComicsDB.COLUMN_READ_PERCENT)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(ComicsDB.COLUMN_PARENT)));
        }
        return null;
    }

    public boolean isInitialized() {
        return mContext != null;
    }

    public void init(Context context) {
        mContext = context;
        mDB = new ComicsDB(mContext);
    }

    private void open() {
        mDatabase = mDB.getWritableDatabase();
    }

    private void close() {
        mDatabase.close();
    }

    public BookFile[] loadBooks() {
        final ArrayList<Long> forRemove = new ArrayList<Long>();
        try {
            open();
            List<BookFile> files = new ArrayList<BookFile>();
            Cursor cursor = mDatabase.query(ComicsDB.TABLE_BOOKS, null, null, null, null, null, null);
            if (cursor.getCount() == 0) return null;
            cursor.moveToFirst();
            do {
                BookFile bookFile = convertToBookFile(cursor);
                if (bookFile == null) {
                    forRemove.add(cursor.getLong(cursor.getColumnIndexOrThrow(ComicsDB.COLUMN_ID)));
                } else {
                    files.add(bookFile);
                }
            } while (cursor.moveToNext());
            cursor.close();
            return files.toArray(new BookFile[files.size()]);
        } finally {
            close();
            new Thread() {
                @Override
                public void run() {
                    super.run();
                    if (forRemove.size() > 0) {
                        removeRows(forRemove.toArray(new Long[forRemove.size()]));
                    }
                }
            }.start();

        }
    }

    public ArrayList<String> getRecent() {
        try {
            ArrayList<String> out = new ArrayList<>();
            open();
            Cursor cursor = mDatabase.query(ComicsDB.TABLE_RECENT, null, null, null, null, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    out.add(cursor.getString(cursor.getColumnIndexOrThrow(ComicsDB.COLUMN_PATH)));
                } while (cursor.moveToNext());
            }
            return out;
        } finally {
            close();
        }
    }

    public ArrayList<String> getBooksByCollection(long id) {
        try {
            ArrayList<String> out = new ArrayList<>();
            open();
            Cursor cursor = mDatabase.query(ComicsDB.getCollectionTableName(id), null, null, null, null, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    out.add(cursor.getString(cursor.getColumnIndexOrThrow(ComicsDB.COLUMN_PATH)));
                } while (cursor.moveToNext());
            }
            return out;
        } catch (Exception e) {
            return null;
        } finally {
            close();
        }
    }

    public BookCollection[] loadCollections() {
        try {
            open();
            Cursor cursor = mDatabase.query(ComicsDB.TABLE_COLLECTIONS, null, null, null, null, null, null);
            if (cursor.getCount() == 0) return null;
            BookCollection[] outCollections = new BookCollection[cursor.getCount()];
            cursor.moveToFirst();
            do {
                outCollections[cursor.getPosition()] = convertToCollection(cursor);
            } while (cursor.moveToNext());
            return outCollections;
        } finally {
            close();
        }
    }

    public void removeRows(Long[] ids) {
        for (long id : ids) {
            removeRow(id);
        }
    }

    public void removeRow(long id) {
        try {
            open();
            mDatabase.delete(ComicsDB.TABLE_BOOKS, ComicsDB.COLUMN_ID + " = " + id, null);
        } finally {
            close();
        }
    }

    public void addRow(BookFile book) {
        try {
            open();
            ContentValues values = new ContentValues();
            values.put(ComicsDB.COLUMN_PATH, Utils.encodePath(book.MFile.getPath()));
            values.put(ComicsDB.COLUMN_READ_PERCENT, book.ReadProgress);
            values.put(ComicsDB.COLUMN_SAVED_POSITION, String.valueOf(book.Position));
            book.Id = mDatabase.insert(ComicsDB.TABLE_BOOKS, null, values);
        } finally {
            close();
        }
    }

    public void updateProgress(long id, String progress, String position) {
        try {
            open();
            ContentValues values = new ContentValues();
            values.put(ComicsDB.COLUMN_READ_PERCENT, progress);
            values.put(ComicsDB.COLUMN_SAVED_POSITION, position);
            int update = mDatabase.update(ComicsDB.TABLE_BOOKS, values, ComicsDB.COLUMN_ID + " = " + id, null);
        } finally {
            close();
        }
    }

    public void addCollection(BookCollection collection) {
        try {
            open();
            ContentValues values = new ContentValues();
            values.put(ComicsDB.COLUMN_NAME, collection.Title);
            collection.Id = mDatabase.insert(ComicsDB.TABLE_COLLECTIONS, null, values);
            mDB.createCollectionTable(collection.Id);
        } finally {
            close();
        }
    }

    public void deleteCollection(BookCollection collection) {
        try {
            open();
            mDatabase.delete(ComicsDB.TABLE_COLLECTIONS, ComicsDB.COLUMN_ID + " = " + collection.Id, null);
            mDB.removeCollectionTable(collection.Id);
        } finally {
            close();
        }
    }

    public void addToCollection(BookFile bookFile, long collectionId) {
        try {
            open();
            ContentValues values = new ContentValues();
            values.put(ComicsDB.COLUMN_PATH, bookFile.MFile.getPath());
            long id = mDatabase.insert(ComicsDB.getCollectionTableName(collectionId), null, values);
            Log.e(TAG, "add to collection: " + id);
        } finally {
            close();
        }
    }

    public void removeFromCollection(BookFile bookFile, long collectionId) {
        try {
            open();
            mDatabase.delete(ComicsDB.getCollectionTableName(collectionId), ComicsDB.COLUMN_PATH + "= ?", new String[]{bookFile.MFile.getPath()});
        } finally {
            close();
        }
    }

    public BookCollection convertToCollection(Cursor cursor) {
        return new BookCollection(cursor.getLong(cursor.getColumnIndexOrThrow(ComicsDB.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndexOrThrow(ComicsDB.COLUMN_NAME)));
    }

    public void addToRecent(BookFile bookFile) {
        try {
            open();
            ContentValues values = new ContentValues();
            values.put(ComicsDB.COLUMN_PATH, bookFile.MFile.getPath());
            mDatabase.insert(ComicsDB.TABLE_RECENT, null, values);
        } finally {
            close();
        }
    }

    public void removeFromRecent(BookFile bookFile) {
        try {
            open();
            mDatabase.delete(ComicsDB.TABLE_RECENT, ComicsDB.COLUMN_PATH + " = ?", new String[]{bookFile.MFile.getPath()});
        } finally {
            close();
        }
    }
}
