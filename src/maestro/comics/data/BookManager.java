package maestro.comics.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import maestro.comics.BookFile;
import maestro.comics.MFile;
import maestro.comics.MJavaFile;
import maestro.comics.Utils;
import maestro.comics.finder.ObjectFinder;
import maestro.comics.model.BookCollection;

import java.io.File;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Artyom on 12.08.2014.
 */
public class BookManager {

    public static final String TAG = BookManager.class.getSimpleName();

    public static final long RECENT_COLLECTION = -2;
    public static final long FAVOURITE_COLLECTION = -3;
    public static final long NO_COLLECTION = -4;

    private static final String PREF_SORT_TYPE = "sort_type";
    private static final String PREF_IS_SORT_INVERSE = "sort_inverse";

    private static final int MSG_NOTIFY_LISTENERS = 0;
    private static final String PARAM_EVENT = "param_event";
    private static volatile BookManager instance;
    private HashMap<Object, OnBookManagerEventListener> mEventListeners = new HashMap<Object, OnBookManagerEventListener>();
    private final Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_NOTIFY_LISTENERS:
                    synchronized (mEventListeners) {
                        final BOOK_MANAGER_EVENT event = (BOOK_MANAGER_EVENT) msg.getData().getSerializable(PARAM_EVENT);
                        Collection<OnBookManagerEventListener> listeners
                                = new ArrayList<OnBookManagerEventListener>(mEventListeners.values());
                        for (OnBookManagerEventListener listener : listeners) {
                            listener.onBookManagerEvent(event, msg.obj);
                        }
                        break;
                    }
            }
        }
    };
    private ArrayList<BookFile> mBooks = new ArrayList<BookFile>();
    private ArrayList<BookFile> mRecentBooks = new ArrayList<>();
    private ArrayList<BookCollection> mCollections = new ArrayList<BookCollection>();
    private Context mContext;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mPreferenceEditor;
    private ArrayList<GlobalFileObserver> mGlobalObservers = new ArrayList<GlobalFileObserver>();
    private BOOK_MANAGER_STATE mCurrentState = BOOK_MANAGER_STATE.IDLE;
    private SORT_TYPE mSortType;
    private AtomicBoolean isSortInverse;

    private BookCollection mRecentCollection = new BookCollection(RECENT_COLLECTION, "Recent");

    public static final synchronized BookManager getInstance() {
        BookManager localInstance = instance;
        if (localInstance == null) {
            synchronized (BookManager.class) {
                localInstance = instance;
                if (localInstance == null) {
                    localInstance = instance = new BookManager();
                }
            }
        }
        return localInstance;
    }

    private boolean isInitialized() {
        return mContext != null;
    }

    public void init(Context context) {
        mContext = context;
        mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mPreferenceEditor = mPreferences.edit();
    }

    public void attachListener(Object key, OnBookManagerEventListener listener) {
        synchronized (mEventListeners) {
            if (mEventListeners.containsKey(key)) {
                mEventListeners.remove(key);
            }
            mEventListeners.put(key, listener);
        }
    }

    public void detachListener(Object key) {
        synchronized (mEventListeners) {
            mEventListeners.remove(key);
        }
    }

    public void start() {
        loadBooks();
    }

    public void stop() {
        if (mGlobalObservers != null && mGlobalObservers.size() > 0) {
            for (GlobalFileObserver observer : mGlobalObservers) {
                observer.stopWatching();
            }
        }
    }

    private void loadBooks() {
        if (mCurrentState == BOOK_MANAGER_STATE.IDLE) {
            new Thread() {
                @Override
                public void run() {
                    super.run();
                    mBooks.clear();
                    mCollections.clear();
                    mCurrentState = BOOK_MANAGER_STATE.LOAD;
                    BookCollection[] dbCollections = ComicsDBHelper.getInstance().loadCollections();
                    if (dbCollections != null && dbCollections.length > 0) {
                        mCollections.addAll(Arrays.asList(dbCollections));
                    }
                    BookFile[] dbFiles = ComicsDBHelper.getInstance().loadBooks();
                    if (dbFiles != null && dbFiles.length > 0) {
                        mBooks.addAll(Arrays.asList(dbFiles));
                    }
                    List<String> recent = ComicsDBHelper.getInstance().getRecent();
                    for (String path : recent) {
                        BookFile bookFile = getBookByPath(path);
                        if (bookFile != null) {
                            mRecentBooks.add(bookFile);
                        }
                    }
                    for (BookCollection collection : mCollections) {
                        getBooks(collection.Id);
                    }
                    notifyListeners(BOOK_MANAGER_EVENT.DB_LOADED, null);
                    scan();
                }
            }.start();
        }
    }

    private void scan() {
        mCurrentState = BOOK_MANAGER_STATE.SCAN;
        notifyListeners(BOOK_MANAGER_EVENT.SCAN_START, null);
        File rootFile = Environment.getExternalStorageDirectory();
        prepareObservers(rootFile);
        MFile[] files = Utils.toMFiles(ObjectFinder.find(rootFile, Utils.COMICS_PATTERN));
        if (files != null && files.length > 0) {
            ensureAfterScan(files);
        }
        notifyListeners(BOOK_MANAGER_EVENT.SCAN_END, null);
        mCurrentState = BOOK_MANAGER_STATE.IDLE;
    }

    public void scanIfAllow() {
        if (mCurrentState == BOOK_MANAGER_STATE.IDLE) {
            mCurrentState = BOOK_MANAGER_STATE.SCAN;
            new Thread() {
                @Override
                public void run() {
                    super.run();
                    mCurrentState = BOOK_MANAGER_STATE.SCAN;
                    notifyListeners(BOOK_MANAGER_EVENT.SCAN_START, null);
                    File rootFile = Environment.getExternalStorageDirectory();
                    prepareObservers(rootFile);
                    MFile[] files = Utils.toMFiles(ObjectFinder.find(rootFile, Utils.COMICS_PATTERN));
                    if (files != null && files.length > 0) {
                        ensureAfterScan(files);
                    }
                    notifyListeners(BOOK_MANAGER_EVENT.SCAN_END, null);
                    mCurrentState = BOOK_MANAGER_STATE.IDLE;
                }
            }.start();
        }
    }

    public BOOK_MANAGER_STATE getCurrentState() {
        return mCurrentState;
    }

    private void addBook(MFile file) {
        BookFile bookFile = new BookFile(file);
        ComicsDBHelper.getInstance().addRow(bookFile);
        mBooks.add(bookFile);
        notifyListeners(BOOK_MANAGER_EVENT.BOOK_ADD, bookFile);
    }

    private void deleteBook(String path) {
        if (removeBook(path)) {
            new File(path).delete();
        }
    }

    private boolean removeBook(String path) {
        BookFile file = getBookByPath(path);
        if (file != null) {
            ComicsDBHelper.getInstance().removeRow(file.Id);
            mBooks.remove(file);
            notifyListeners(BOOK_MANAGER_EVENT.BOOK_DELETE, file);
            return true;
        }
        return false;
    }

    public void addCollection(String collectionName) {
        BookCollection collection = new BookCollection(collectionName);
        ComicsDBHelper.getInstance().addCollection(collection);
        mCollections.add(collection);
        notifyListeners(BOOK_MANAGER_EVENT.COLLECTION_ADD, collection);
    }

    public void deleteCollection(BookCollection collection) {
        mCollections.remove(collection);
        ComicsDBHelper.getInstance().deleteCollection(collection);
        notifyListeners(BOOK_MANAGER_EVENT.COLLECTION_DELETE, collection);
    }

    public void addToCollection(BookFile file, BookCollection collection) {
        ComicsDBHelper.getInstance().addToCollection(file, collection.Id);
        file.setCollection(collection);
        notifyListeners(BOOK_MANAGER_EVENT.BOOK_ADD_TO_COLLECTION, collection);
    }

    public void removeFromCollection(BookFile file, BookCollection collection) {
        ComicsDBHelper.getInstance().addToCollection(file, collection.Id);
        file.setCollection(null);
        notifyListeners(BOOK_MANAGER_EVENT.BOOK_REMOVED_FROM_COLLECTION, collection);
    }

    public synchronized void addToCollection(ArrayList<Object> objects, BookCollection toCollection) {
        ArrayList<BookCollection> mNotifyCollections = new ArrayList<>();
        for (Object obj : objects) {
            if (obj instanceof BookFile) {
                BookFile bookFile = (BookFile) obj;
                BookCollection collection = getCollectionById(bookFile.ParentId);
                if (!mNotifyCollections.contains(collection)) {
                    mNotifyCollections.add(collection);
                }
                bookFile.setCollection(toCollection);
                ComicsDBHelper.getInstance().addToCollection(bookFile, toCollection.Id);
            }
        }
        for (BookCollection collection : mNotifyCollections) {
            notifyListeners(BOOK_MANAGER_EVENT.BOOK_REMOVED_FROM_COLLECTION, collection);
        }
        notifyListeners(BOOK_MANAGER_EVENT.BOOK_ADD, toCollection);
    }

    public synchronized void removeFromCollection(ArrayList<Object> objects) {
        ArrayList<BookCollection> mNotifyCollections = new ArrayList<>();
        for (Object obj : objects) {
            if (obj instanceof BookFile) {
                BookFile bookFile = (BookFile) obj;
                BookCollection collection = bookFile.Collection;
                if (collection != null) {
                    if (!mNotifyCollections.contains(collection)) {
                        mNotifyCollections.add(collection);
                    }
                    bookFile.setCollection(null);
                    ComicsDBHelper.getInstance().removeFromCollection(bookFile, collection.Id);
                }
            }
        }
        for (BookCollection collection : mNotifyCollections) {
            notifyListeners(BOOK_MANAGER_EVENT.BOOK_REMOVED_FROM_COLLECTION, collection);
        }
    }

    public void notifyListeners(BOOK_MANAGER_EVENT event, Object obj) {
        Message msg = uiHandler.obtainMessage(MSG_NOTIFY_LISTENERS);
        msg.getData().putSerializable(PARAM_EVENT, event);
        msg.obj = obj;
        uiHandler.sendMessage(msg);
    }

    public ArrayList<BookFile> getRecentBooks() {
        ArrayList<BookFile> bookFiles = (ArrayList<BookFile>) mRecentBooks.clone();
        return bookFiles;
    }

    public BookFile[] getBooks(long collectionId) {
        if (collectionId == NO_COLLECTION) {
            Collections.sort(mBooks, getSortType() == SORT_TYPE.NAME ? new NameComparator(isSortInverse())
                    : new DateComparator(isSortInverse()));
            return mBooks.toArray(new BookFile[mBooks.size()]);
        } else {
            BookCollection collection = getCollectionById(collectionId);
            ArrayList<String> paths = ComicsDBHelper.getInstance().getBooksByCollection(collectionId);
            if (paths != null && paths.size() > 0) {
                ArrayList<BookFile> outBooks = new ArrayList<>();
                for (String path : paths) {
                    for (BookFile bookFile : mBooks) {
                        if (bookFile.MFile.getPath().equals(path)) {
                            bookFile.Collection = collection;
                            outBooks.add(bookFile);
                        }
                    }
                }
                return outBooks.toArray(new BookFile[outBooks.size()]);
            }
            return null;
        }
    }

    public BookFile getBookByPath(String path) {
        for (BookFile file : mBooks) {
            if (file.MFile.getPath().equals(path))
                return file;
        }
        File file = new File(path);
        if (file.exists()) {
            BookFile bookFile = new BookFile(new MJavaFile(file, file.getParentFile()));
            addBook(bookFile.MFile);
            return bookFile;
        }
        return null;
    }

    public BookFile getBookById(long id) {
        for (BookFile file : mBooks) {
            if (file.Id == id)
                return file;
        }
        return null;
    }

    public BookCollection[] getCollections() {
        return mCollections.toArray(new BookCollection[mCollections.size()]);
    }

    public BookCollection getCollectionById(long id) {
        if (id == RECENT_COLLECTION) {
            return mRecentCollection;
        }

        for (BookCollection collection : mCollections) {
            if (collection.Id == id) {
                return collection;
            }
        }
        return null;
    }

    public BookCollection getRecentCollection() {
        return mRecentCollection;
    }

    private MFile[] ensureAfterScan(MFile[] files) {
        ArrayList<MFile> out = new ArrayList<MFile>();
        for (MFile file : files) {
            boolean have = false;
            for (BookFile book : mBooks) {
                if (book.MFile.getPath().equals(file.getPath())) {
                    have = true;
                    break;
                }
            }
            if (!have) {
                addBook(file);
            }
        }
        return out.toArray(new MFile[out.size()]);
    }

    private void prepareObservers(File root) {
        mGlobalObservers.add(new GlobalFileObserver(root.getPath()));
        File[] files = root.listFiles();
        if (files != null && files.length > 0)
            for (File file : files) {
                if (file.isDirectory()) {
                    prepareObservers(file);
                }
            }
    }

    public void updateBookProgress(long id, String progress, String position) {
        updateBookProgress(getBookById(id), progress, position);
    }

    public void updateBookProgress(BookFile bookFile, String progress, String position) {
        bookFile.Position = Integer.valueOf(position);
        bookFile.ReadProgress = progress;
        ComicsDBHelper.getInstance().updateProgress(bookFile.Id, progress, position);
    }

    public void addToRecent(BookFile bookFile) {
        if (mRecentBooks.contains(bookFile)) {
            removeFromRecent(bookFile);
        }
        ComicsDBHelper.getInstance().addToRecent(bookFile);
        mRecentBooks.add(bookFile);
    }

    public void removeFromRecent(BookFile bookFile) {
        ComicsDBHelper.getInstance().removeFromRecent(bookFile);
        mRecentBooks.remove(bookFile);

    }

    public SORT_TYPE getSortType() {
        return mSortType != null ? mSortType
                : (mSortType = SORT_TYPE.valueOf(mPreferences.getString(PREF_SORT_TYPE, SORT_TYPE.NAME.name())));

    }

    public void setSortType(SORT_TYPE sortType) {
        mPreferenceEditor.putString(PREF_SORT_TYPE, sortType.name()).apply();
        mSortType = sortType;

    }

    public boolean isSortInverse() {
        return isSortInverse != null ? isSortInverse.get()
                : (isSortInverse = new AtomicBoolean(mPreferences.getBoolean(PREF_IS_SORT_INVERSE, false))).get();

    }

    public void setSortInverse(boolean inverse) {
        mPreferenceEditor.putBoolean(PREF_IS_SORT_INVERSE, inverse).apply();
        if (isSortInverse == null)
            isSortInverse = new AtomicBoolean();
        isSortInverse.set(inverse);

    }

    public void makeCollections() {
        LinkedHashMap<String, ArrayList<BookFile>> mFiles = new LinkedHashMap<>();
//        for (int i = 0; i < ; i++) {
//
//        }

    }

    public enum BOOK_MANAGER_EVENT {
        BOOK_ADD, BOOK_DELETE, DB_LOADED, SCAN_START, SCAN_END, COLLECTION_ADD, COLLECTION_DELETE,
        BOOK_ADD_TO_COLLECTION, BOOK_REMOVED_FROM_COLLECTION
    }

    public enum BOOK_MANAGER_STATE {
        LOAD, SCAN, IDLE
    }

    public enum SORT_TYPE {
        NAME, DATE
    }

    public interface OnBookManagerEventListener {
        public void onBookManagerEvent(BOOK_MANAGER_EVENT event, Object object);
    }

    public static class NameComparator implements Comparator<BookFile> {

        private final boolean isInverse;

        public NameComparator(boolean inverse) {
            isInverse = inverse;
        }

        @Override
        public int compare(BookFile lhs, BookFile rhs) {
            if (!isInverse) {
                return lhs.MFile.getName().toLowerCase().compareTo(rhs.MFile.getName().toLowerCase());
            } else {
                return rhs.MFile.getName().toLowerCase().compareTo(lhs.MFile.getName().toLowerCase());
            }
        }

    }

    public static class DateComparator implements Comparator<BookFile> {

        private final boolean isInverse;

        public DateComparator(boolean inverse) {
            isInverse = inverse;
        }

        @Override
        public int compare(BookFile lhs, BookFile rhs) {
            Date lhsDate = new Date(lhs.MFile.getTime());
            Date rhsDate = new Date(rhs.MFile.getTime());
            if (!isInverse) {
                return lhsDate.compareTo(rhsDate);
            } else {
                return rhsDate.compareTo(lhsDate);
            }
        }
    }

}
