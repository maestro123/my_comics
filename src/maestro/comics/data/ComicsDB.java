package maestro.comics.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by artyom on 8/11/14.
 */
public class ComicsDB extends SQLiteOpenHelper {

    public static final String TABLE_BOOKS = "books";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_PATH = "_path";
    public static final String COLUMN_SAVED_POSITION = "_position";
    public static final String COLUMN_READ_PERCENT = "_read_percent";
    public static final String COLUMN_PARENT = "_parent";

    private static final String COLLECTION_TABLE_NAME = "collection%d";

    private static final String CREATE_TABLE_BOOKS = "create table "
            + TABLE_BOOKS + "(" + COLUMN_ID
            + " integer primary key autoincrement, " + COLUMN_PATH
            + " TEXT, " + COLUMN_SAVED_POSITION +
            " TEXT, " + COLUMN_READ_PERCENT + " TEXT, "
            + COLUMN_PARENT + " integer DEFAULT -1);";

    public static final String TABLE_COLLECTIONS = "collections";
    public static final String COLUMN_NAME = "_name";

    private static final String CREATE_TABLE_COLLECTIONS = "create table "
            + TABLE_COLLECTIONS + "(" + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_NAME + " TEXT);";

    public static final String TABLE_RECENT = "recents";

    private static final String CREATE_TABLE_RECENT = "create table "
            + TABLE_RECENT + "(" + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_PATH + " TEXT);";

    private static final String CREATE_COLLECTION_TABLE = "create table %s"
            + "(" + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_PATH + " TEXT);";

    private static final String DATABASE_NAME = "comics.db";
    private static final int DATABASE_VERSION = 1;

    public ComicsDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_BOOKS);
        sqLiteDatabase.execSQL(CREATE_TABLE_COLLECTIONS);
        sqLiteDatabase.execSQL(CREATE_TABLE_RECENT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_BOOKS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_COLLECTIONS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_RECENT);
        onCreate(sqLiteDatabase);
    }

    public void createCollectionTable(long id) {
        getWritableDatabase().beginTransaction();
        getWritableDatabase().execSQL(String.format(CREATE_COLLECTION_TABLE, getCollectionTableName(id)));
        getWritableDatabase().setTransactionSuccessful();
        getWritableDatabase().endTransaction();
    }

    public void removeCollectionTable(long id) {
        getWritableDatabase().beginTransaction();
        getWritableDatabase().execSQL("DROP TABLE IF EXISTS " + getCollectionTableName(id));
        getWritableDatabase().setTransactionSuccessful();
        getWritableDatabase().endTransaction();
    }

    public static String getCollectionTableName(long id){
        return String.format(COLLECTION_TABLE_NAME, id);
    }

}
