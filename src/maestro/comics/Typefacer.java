package maestro.comics;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by artyom on 8/13/14.
 */
public class Typefacer {

    private static volatile Typefacer instance;

    public static Typefacer getInstance() {
        if (instance == null) {
            instance = new Typefacer();
        }
        return instance;
    }

    Typefacer() {
    }

    public static Typeface rCondensedRegular;
    public static Typeface rLight;
    public static Typeface rRegular;
    public static Typeface Lobster;

    public void init(Context context) {
        rCondensedRegular = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Regular.ttf");
        rLight = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
        rRegular = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
        Lobster = Typeface.createFromAsset(context.getAssets(), "fonts/Lobster.ttf");
    }

}
