package maestro.comics;

import android.content.Context;
import maestro.support.v1.svg.SVG;
import maestro.support.v1.svg.SVGHelper;

/**
 * Created by artyom on 8.5.15.
 */
public class SVGInstance {

    private static SVGInstance instance;

    public static SVGInstance getInstance() {
        return instance != null ? instance : (instance = new SVGInstance());
    }

    private static SVGHelper.SVGHolder mHolder;

    SVGInstance() {
    }

    public void init(Context context) {
        SVGHelper.init(context);
        mHolder = new SVGHelper.SVGHolder(context.getResources());
    }

    public static SVGHelper.SVGHolder get() {
        return mHolder;
    }


}
