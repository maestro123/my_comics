package maestro.comics;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Artyom on 5/13/2015.
 */
public class Settings {

    private static volatile Settings instance;

    public static synchronized Settings getInstance() {
        return instance != null ? instance : (instance = new Settings());
    }

    Settings() {
    }

    private Context mContext;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;

    public void init(Context context) {
        mContext = context;
        mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mEditor = mPreferences.edit();
    }

    private static final String PREF_PAGE_MODE = "pref_page_mode";
    private static final String PREF_BACKGROUND_COLOR = "pref_background_color";
    private static final String PREF_AUTH_BACKGROUND = "pref_auto_background";

    public enum PAGE_MODE {
        SINGLE, DOUBLE
    }

    public void setPageMode(PAGE_MODE mode) {
        mEditor.putString(PREF_PAGE_MODE, mode.name()).apply();
    }

    public PAGE_MODE getPageMode() {
        return PAGE_MODE.valueOf(mPreferences.getString(PREF_PAGE_MODE, PAGE_MODE.SINGLE.name()));
    }

    public void setBackgroundColor(int color) {
        mEditor.putInt(PREF_BACKGROUND_COLOR, color).apply();
    }

    public int getBackgroundColor() {
        return mPreferences.getInt(PREF_BACKGROUND_COLOR, -1);
    }

    public void setAutoBackground(boolean autoBackground) {
        mEditor.putBoolean(PREF_AUTH_BACKGROUND, autoBackground).apply();
    }

    public boolean isAutoBackground() {
        return mPreferences.getBoolean(PREF_AUTH_BACKGROUND, true);
    }

}
