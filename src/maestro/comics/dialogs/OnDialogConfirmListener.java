package maestro.comics.dialogs;

import android.view.View;

/**
 * Created by artyom on 8/13/14.
 */
public interface OnDialogConfirmListener {
    public void onDialogConfirm(View view, Object object, int id);
}
