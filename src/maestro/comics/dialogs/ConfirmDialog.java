package maestro.comics.dialogs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import maestro.comics.R;
import maestro.comics.Typefacer;

/**
 * Created by Artyom on 15.08.2014.
 */
public class ConfirmDialog extends BaseDialogFragment implements View.OnClickListener {

    public static final String TAG = ConfirmDialog.class.getSimpleName();

    public static final int NO_ID = -1;
    public static final int REMOVE_FROM_COLLECTIONS_ID = 0;
    public static final int DELETE_COLLECTION_ID = 1;

    private static final String PARAM_TEXT = "param_text";
    private static final String PARAM_ID = "param_id";
    private TextView txtText;
    private Button btnOk, btnCancel;
    private OnDialogConfirmListener mConfirmListener;

    public static ConfirmDialog makeInstance(String text) {
        return makeInstance(text, NO_ID);
    }

    public static ConfirmDialog makeInstance(String text, int id) {
        ConfirmDialog dialog = new ConfirmDialog();
        Bundle args = new Bundle(2);
        args.putString(PARAM_TEXT, text);
        args.putInt(PARAM_ID, id);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.confirm_dialog_view, null);
        txtText = (TextView) v.findViewById(R.id.text);
        btnOk = (Button) v.findViewById(R.id.btn_ok);
        btnCancel = (Button) v.findViewById(R.id.btn_cancel);
        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        txtText.setTypeface(Typefacer.rCondensedRegular);
        btnOk.setTypeface(Typefacer.rCondensedRegular);
        btnCancel.setTypeface(Typefacer.rCondensedRegular);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        txtText.setText(getArguments().getString(PARAM_TEXT));
        if (getParentFragment() instanceof OnDialogConfirmListener) {
            mConfirmListener = (OnDialogConfirmListener) getParentFragment();
        } else if (getActivity() instanceof OnDialogConfirmListener) {
            mConfirmListener = (OnDialogConfirmListener) getActivity();
        }
    }

    public void setOnDialogConfirmListener(OnDialogConfirmListener listener) {
        mConfirmListener = listener;
    }

    @Override
    public void onClick(View view) {
        final int id = view.getId();
        switch (id) {
            case R.id.btn_ok:
                if (mConfirmListener != null)
                    mConfirmListener.onDialogConfirm(view, null, getArguments() != null ? getArguments().getInt(PARAM_ID, NO_ID) : NO_ID);
            case R.id.btn_cancel:
                dismiss();
                return;
        }
    }

}
