package maestro.comics.dialogs;

/**
 * Created by artyom on 8/13/14.
 */
public interface InputEnsurer {
    public String ensure(String text);
}
