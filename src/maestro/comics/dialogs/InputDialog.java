package maestro.comics.dialogs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import maestro.comics.R;
import maestro.comics.Typefacer;

/**
 * Created by artyom on 8/13/14.
 */
public class InputDialog extends BaseDialogFragment implements View.OnClickListener {

    public static final String TAG = InputDialog.class.getSimpleName();

    private Button btnOk, btnCancel;
    private EditText mEditText;
    private TextView txtTitle;
    private OnDialogConfirmListener mConfirmListener;
    private InputEnsurer mEnsurer;

    public static final InputDialog makeInstance(String title) {
        InputDialog dialog = new InputDialog();
        Bundle args = new Bundle(1);
        args.putString(PARAM_TITLE, title);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.input_dialog_view, null);
        btnOk = (Button) v.findViewById(R.id.btn_ok);
        btnCancel = (Button) v.findViewById(R.id.btn_cancel);
        mEditText = (EditText) v.findViewById(R.id.edit_text);
        txtTitle = (TextView) v.findViewById(R.id.title);
        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnOk.setTypeface(Typefacer.rCondensedRegular);
        btnCancel.setTypeface(Typefacer.rCondensedRegular);
        txtTitle.setTypeface(Typefacer.rCondensedRegular);
        return v;
    }

    public void setInputEnsurer(InputEnsurer ensurer) {
        mEnsurer = ensurer;
    }

    public void setOnDialogConfirmListener(OnDialogConfirmListener listener) {
        mConfirmListener = listener;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        txtTitle.setText(getArguments() != null ? getArguments().getString(PARAM_TITLE) : null);
    }

    @Override
    public void onClick(View view) {
        final int id = view.getId();
        switch (id) {
            case R.id.btn_ok:
                String text = mEditText.getText() != null ? mEditText.getText().toString() : null;
                if (mEnsurer != null) {
                    String error = mEnsurer.ensure(text);
                    if (error != null) {
                        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                if (mConfirmListener != null)
                    mConfirmListener.onDialogConfirm(view, text, -1);
            case R.id.btn_cancel:
                dismiss();
                return;
        }
    }
}
