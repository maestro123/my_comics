package maestro.comics;

import maestro.rar.RarEntryFile;
import maestro.rar.RarFile;

import java.io.InputStream;

/**
 * Created by Artyom on 31.07.2014.
 */
public class MRarArchiveFile implements MFile {

    private RarEntryFile entryFile;
    private RarFile parent;

    public MRarArchiveFile(RarEntryFile entryFile, RarFile parent) {
        this.entryFile = entryFile;
        this.parent = parent;
    }

    @Override
    public String getName() {
        return entryFile.getName();
    }

    @Override
    public String getKey() {
        return entryFile.getName();
    }

    @Override
    public String getPath() {
        return null;
    }

    @Override
    public Object getParent() {
        return parent;
    }

    @Override
    public boolean isDirectory() {
        return false;
    }

    @Override
    public long getSize() {
        return entryFile.getUnpackedSize();
    }

    @Override
    public long getTime() {
        return entryFile.getTimestamp();
    }

    @Override
    public InputStream getStream() {
        return parent.getInputStream(entryFile.getName());
    }
}
