package maestro.comics.read;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.*;
import android.widget.*;
import com.dream.android.mim.ImageLoadObject;
import com.dream.android.mim.MIM;
import com.dream.android.mim.MIMUtils;
import maestro.comics.*;
import maestro.comics.data.BookManager;
import maestro.comics.ui.ZoomImageView;

import java.util.List;

/**
 * Created by Artyom on 31.07.2014.
 */
public class ReadActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    public static final String TAG = ReadActivity.class.getSimpleName();

    public static final String PARAM_PATH = "param_path";

    private static final int CHANGE_PAGE_MODE = 0;
    private static final int CHANGE_BACKGROUND_COLOR = 1;

    private ViewPager mPager;
    private TextView pageCurrent;
    private TextView pageTotal;
    private TextView pagePosition;
    private Toolbar mToolbar;
    private SeekBar mPositionSeekBar;
    private View mPositionSeekBarParent;
    private View mTopShadow, mBottomShadow;
    private ComicsPagerAdapter mAdapter;
    private BaseBookReader mBookReader;
    private BookFile book;
    private boolean isUIVisible;

    private float mTotalZoom = 1f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT > 20) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(MIMUtils.blendColors(getResources().getColor(R.color.indicator_color), Color.parseColor("#000000"), 0.75f));
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.read_view);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        mToolbar.setNavigationIcon(SVGInstance.get().getDrawable(R.raw.ic_back, Color.WHITE));
        setSupportActionBar(mToolbar);

        for (int i = 0; i < mToolbar.getChildCount(); i++) {
            if (mToolbar.getChildAt(i) instanceof TextView) {
                TextView mTextView = (TextView) mToolbar.getChildAt(i);
                mTextView.setTextColor(Color.WHITE);
                mTextView.setTypeface(Typefacer.rCondensedRegular);
            }
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setOffscreenPageLimit(4);
        mPositionSeekBarParent = findViewById(R.id.seek_bar_parent);
        mPositionSeekBar = (SeekBar) findViewById(R.id.seek_bar);
        mPositionSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (pageCurrent != null) {
                    pageCurrent.setText(String.valueOf(progress + 1));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mPager.setCurrentItem(seekBar.getProgress(), false);
            }
        });
        pageCurrent = (TextView) findViewById(R.id.page_read);
        pageTotal = (TextView) findViewById(R.id.page_total);

        mTopShadow = findViewById(R.id.toolbar_shadow);
        mBottomShadow = findViewById(R.id.seek_bar_shadow);

        processIntent(getIntent(), savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        processIntent(intent, null);
    }

    private void processIntent(Intent intent, Bundle savedInstanceState){
        String path = null;
        if (getIntent() != null) {
            if (intent.hasExtra(PARAM_PATH)) {
                path = intent.getStringExtra(PARAM_PATH);
            } else {
                Uri uri = intent.getData();
                path = uri.getPath();
            }
        }
        if (Utils.isRar(path)) {
            mBookReader = RarBookReader.getInstance();
        } else if (Utils.isArchive(path)) {
            mBookReader = ZipBookReader.getInstance();
        } else {
            Toast.makeText(this, R.string.file_is_not_suppported, Toast.LENGTH_SHORT);
            finish();
            return;
        }
        book = BookManager.getInstance().getBookByPath(path);
        if (book == null) {
            Toast.makeText(this, R.string.book_not_found, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        mBookReader.loadBook(path);
        BookManager.getInstance().addToRecent(book);

        mPager.setAdapter(mAdapter = new ComicsPagerAdapter(getSupportFragmentManager()));
        mPager.setOnPageChangeListener(this);
        mPositionSeekBar.setMax(mAdapter.getCount() - 1);
        if (savedInstanceState == null) {
            mPager.setCurrentItem(book.Position);
            mPositionSeekBar.setProgress(book.Position);
        }
        updateReadProgress();
        hideUI();
    }

    @Override
    public void onPageScrolled(int i, float v, int i2) {

    }

    @Override
    public void onPageSelected(int i) {
        mPositionSeekBar.setProgress(i);
        updateReadProgress();

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    public BaseBookReader getReader() {
        return mBookReader;
    }

    private void updateReadProgress() {
        if (pageCurrent != null) {
            pageCurrent.setText(String.valueOf(mPager.getCurrentItem() + 1));
        }
        if (pageTotal != null) {
            pageTotal.setText(String.valueOf(mAdapter.getCount()));
        }
        BookManager.getInstance().updateBookProgress(book,
                String.valueOf((mPager.getCurrentItem() + 1) + "/" + mAdapter.getCount()),
                String.valueOf(mPager.getCurrentItem()));
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideUI();
        }
    }

    private final void applyChange(int change) {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null && fragments.size() > 0) {
            for (Fragment fragment : fragments) {
                if (fragment instanceof PageFragment) {
                    ((PageFragment) fragment).applyChange(change);
                }
            }
        }
    }

    public void hideUI() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        final int height = getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material);
        final int shadowHeight = height + getResources().getDimensionPixelSize(R.dimen.shadow_height);
        mPositionSeekBarParent.animate().alpha(0).translationY(height).setDuration(150).start();
        mToolbar.animate().alpha(0).translationY(-height).setDuration(150).start();
        mTopShadow.animate().alpha(0).translationY(-shadowHeight).setDuration(150).start();
        mBottomShadow.animate().alpha(0).translationY(shadowHeight).setDuration(150).start();
        isUIVisible = false;
    }

    public void showUI() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        mPositionSeekBarParent.animate().alpha(1f).translationY(0).setDuration(150).start();
        mToolbar.animate().alpha(1f).translationY(0).setDuration(150).start();
        mBottomShadow.animate().alpha(1f).translationY(0).setDuration(150).start();
        mTopShadow.animate().alpha(1f).translationY(0).setDuration(150).start();
        isUIVisible = true;
    }

    public void toggleUI() {
        if (isUIVisible)
            hideUI();
        else
            showUI();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mBookReader != null)
            mBookReader.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.read_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem doubleItem = menu.findItem(R.id.page_mode_double);
        MenuItem singleItem = menu.findItem(R.id.page_mode_single);
        Settings.PAGE_MODE pageMode = Settings.getInstance().getPageMode();
        doubleItem.setChecked(pageMode == Settings.PAGE_MODE.DOUBLE);
        singleItem.setChecked(pageMode == Settings.PAGE_MODE.SINGLE);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.page_mode_double || item.getItemId() == R.id.page_mode_single) {
            Settings.PAGE_MODE mode = item.getItemId() == R.id.page_mode_double ? Settings.PAGE_MODE.DOUBLE : Settings.PAGE_MODE.SINGLE;
            if (mode != Settings.getInstance().getPageMode()) {
                Settings.getInstance().setPageMode(mode);
                if (mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                }
                mPositionSeekBar.setMax(mAdapter.getCount() - 1);
                mPager.setCurrentItem(mode == Settings.PAGE_MODE.DOUBLE ? mPager.getCurrentItem() / 2 : mPager.getCurrentItem() * 2, false);
            }
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    public static class PageFragment extends Fragment implements ImageLoadObject.OnImageLoadEventListener,
            ZoomImageView.OnZoomChangeListener, View.OnClickListener {

        private static final String PARAM_POSITION = "param_position";
        private ZoomImageView imageView;
        private ProgressBar mProgress;
        private Button btnReload;

        private ReadActivity mActivity;

        public static PageFragment makeInstance(int position) {
            PageFragment fragment = new PageFragment();
            Bundle args = new Bundle(1);
            args.putInt(PARAM_POSITION, position);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            mActivity = (ReadActivity) activity;
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View v = inflater.inflate(R.layout.comics_page_view, null);
            imageView = (ZoomImageView) v.findViewById(R.id.image);
            mProgress = (ProgressBar) v.findViewById(R.id.progress);
            btnReload = (Button) v.findViewById(R.id.btn_reload);
            imageView.setOnZoomChangeListener(this);
            btnReload.setOnClickListener(this);
            v.setOnClickListener(this);
            imageView.setOnClickListener(this);
            return v;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            imageView.setZoom(mActivity.getTotalZoom());
            loadImage();
        }

        @Override
        public void onDestroy() {
            ImageLoadObject.cancel(imageView);
            super.onDestroy();
        }

        private void loadImage() {
            final DisplayMetrics metrics = getResources().getDisplayMetrics();
            BaseBookReader.BookReadItemLoadObject archiveObject = mActivity.getReader().
                    getLoadObjectAtPosition(Settings.getInstance().getPageMode(), getArguments().getInt(PARAM_POSITION));
            MIM.by(Utils.MIM_COMICS).to(imageView, archiveObject.Key + Settings.getInstance().getPageMode())
                    .analyze(true)
                    .object(archiveObject.Object)
                    .size(metrics.widthPixels, metrics.heightPixels)
                    .listener(this).async();
        }

        @Override
        public void onImageLoadEvent(IMAGE_LOAD_EVENT event, ImageLoadObject loadObject) {
            switch (event) {
                case FINISH:
                    mProgress.setVisibility(View.GONE);
                    btnReload.setVisibility(View.GONE);
                    Integer[] colors = loadObject.getAnalyzedColors();
                    if (colors != null && colors.length > 0) {
                        imageView.setBackgroundColor(colors[colors.length - 1]);
                    }
                    break;
                case START:
                    mProgress.setVisibility(View.VISIBLE);
                    btnReload.setVisibility(View.GONE);
                    break;
            }
        }

        private void showError() {
            btnReload.setVisibility(View.VISIBLE);
        }

        @Override
        public void onZoomChanging(float zoom) {

        }

        @Override
        public void onZoomChanged(float zoom) {
            mActivity.setTotalZoom(zoom, this);
        }

        @Override
        public void onMove(float distanceX, float distanceY) {

        }

        @Override
        public void onClick(View view) {
            final int id = view.getId();
            switch (id) {
                case R.id.btn_reload:
                    loadImage();
                    break;
                default:
                    mActivity.toggleUI();
            }
        }

        public void setZoom(float zoom, float fx, float fy) {
            if (imageView != null) {
                imageView.setZoom(zoom, 0f, 0f);
            }
        }

        public void applyChange(int change) {
            switch (change) {
                case CHANGE_PAGE_MODE:
                    loadImage();
                    break;
            }
        }
    }

    private class ComicsPagerAdapter extends FragmentStatePagerAdapter {

        public ComicsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return PageFragment.makeInstance(i);
        }

        @Override
        public int getCount() {
            return mBookReader.getCount(Settings.getInstance().getPageMode());
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }

    public void setTotalZoom(float zoom, Fragment fragment) {
        mTotalZoom = zoom;
        sendZoom(fragment);
    }

    private void sendZoom(Fragment sendFragment) {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null && fragments.size() > 0) {
            for (Fragment fragment : fragments) {
                if (fragment instanceof PageFragment && fragment != sendFragment) {
                    ((PageFragment) fragment).setZoom(mTotalZoom, 0, 0);
                }
            }
        }
    }

    private float getTotalZoom() {
        return mTotalZoom;
    }

}
