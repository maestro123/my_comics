package maestro.comics.read;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Environment;
import android.util.Log;
import com.dream.android.mim.ImageLoadObject;
import com.dream.android.mim.MIMDefaultMaker;
import maestro.comics.*;

import java.io.File;

/**
 * Created by Artyom on 31.07.2014.
 */
public class MComicsPageMaker extends MIMDefaultMaker {

    public static final String TAG = MComicsPageMaker.class.getSimpleName();

    @Override
    public Bitmap getBitmap(ImageLoadObject loadObject, Context ctx) {
        Object object = loadObject.getObject();
        if (object instanceof MRarArchiveFile) {
            return getBitmap(loadObject, (MFile) object);
        } else if (object instanceof MZipArchiveFile) {
            return getBitmap(loadObject, (MFile) object);
        } else if (object instanceof BookFile) {
            return ofMFile(loadObject, ((BookFile) object).MFile);
        } else if (object instanceof MFile) {
            return ofMFile(loadObject, (MFile) object);
        } else if (object instanceof MFile[]) {
            MFile[] files = (MFile[]) object;
            Bitmap first = getBitmap(loadObject, files[0]);
            if (files[1] == null) {
                return first;
            }
            Bitmap second = getBitmap(loadObject, files[1]);
            return appendBitmaps(first, second);
        }
        return null;
    }

    private Bitmap appendBitmaps(Bitmap firstBitmap, Bitmap secondBitmap) {
        final int outWidth = firstBitmap.getWidth() + secondBitmap.getWidth();
        final int outHeight = Math.max(firstBitmap.getHeight(), secondBitmap.getHeight());
        Bitmap outBitmap = Bitmap.createBitmap(outWidth, outHeight, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(outBitmap);
        canvas.drawBitmap(firstBitmap, 0, 0, null);
        int fWidth = firstBitmap.getWidth();
        firstBitmap.recycle();
        canvas.drawBitmap(secondBitmap, fWidth, 0, null);
        secondBitmap.recycle();
        return outBitmap;
    }

    private final Bitmap ofMFile(ImageLoadObject loadObject, MFile mFile) {
        MFile[] files = Utils.collectMFileFromArchive(new File(mFile.getPath()), true);
        if (files != null && files.length > 0) {
            Bitmap bitmap;
            for (int i = 0; i < files.length; i++) {
                bitmap = getBitmap(loadObject, files[i]);
                if (bitmap != null) {
                    return bitmap;
                }
            }
        }
        return null;
    }

    private final Bitmap getBitmap(ImageLoadObject loadObject, MFile file) {
        try {
            BitmapFactory.Options opt = new BitmapFactory.Options();
            opt.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(file.getStream(), new Rect(), opt);
            opt.inSampleSize = calculateInSampleSize(opt, loadObject.getWidth(), loadObject.getHeight());
            opt.inJustDecodeBounds = false;
            return BitmapFactory.decodeStream(file.getStream(), new Rect(), opt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
