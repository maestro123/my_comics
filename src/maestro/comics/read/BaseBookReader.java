package maestro.comics.read;

import maestro.comics.MFile;
import maestro.comics.Settings;

import java.io.File;

/**
 * Created by Artyom on 01.08.2014.
 */
public abstract class BaseBookReader {

    public void loadBook(File file) {
        loadBook(file.getPath());
    }

    public void loadBook(MFile file) {
        loadBook(file.getPath());
    }

    public abstract void loadBook(String path);

    public abstract int getCount(Settings.PAGE_MODE pageMode);

    public abstract void close();

    public abstract BookReadItemLoadObject getLoadObjectAtPosition(Settings.PAGE_MODE pageMode, int position);

    public static class BookReadItemLoadObject {
        public String Key;
        public Object Object;

        public BookReadItemLoadObject(String key, Object object) {
            Key = key;
            Object = object;
        }

    }

}
