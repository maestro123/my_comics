package maestro.comics.read;

import maestro.comics.MRarArchiveFile;
import maestro.comics.Settings;
import maestro.comics.Utils;
import maestro.rar.RarEntryFile;
import maestro.rar.RarFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Artyom on 31.07.2014.
 */
public class RarBookReader extends BaseBookReader {

    public static final String TAG = RarBookReader.class.getSimpleName();

    private static volatile RarBookReader instance;
    private RarFile mArchive;
    private List<RarEntryFile> mCurrentHeaders;

    public static synchronized RarBookReader getInstance() {
        RarBookReader localInstance = instance;
        if (localInstance == null)
            synchronized (RarBookReader.class) {
                localInstance = instance;
                if (localInstance == null) {
                    localInstance = instance = new RarBookReader();
                }
            }
        return localInstance;
    }

    @Override
    public void loadBook(String path) {
        try {
            mArchive = new RarFile(new File(path));
            final List<RarEntryFile> mLocalHeaders = mArchive.list();
            mCurrentHeaders = new ArrayList<RarEntryFile>();
            for (RarEntryFile header : mLocalHeaders) {
                if (header.getName().toLowerCase().matches(Utils.IMAGE_PATTERN)) {
                    mCurrentHeaders.add(header);
                }
            }
            Collections.sort(mCurrentHeaders, new Utils.FileHeaderComparator());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getCount(Settings.PAGE_MODE page_mode) {
        if (mCurrentHeaders != null) {
            switch (page_mode) {
                case DOUBLE:
                    return (int) Math.ceil(mCurrentHeaders.size() / 2f);
                case SINGLE:
                    return mCurrentHeaders.size();
            }
        }
        return 0;
    }

    @Override
    public void close() {
        mArchive.close();
    }

    @Override
    public BookReadItemLoadObject getLoadObjectAtPosition(Settings.PAGE_MODE pageMode, int position) {
        if (pageMode == Settings.PAGE_MODE.DOUBLE) {
            position *= 2;
        }
        final RarEntryFile header = mCurrentHeaders.get(position);
        if (pageMode == Settings.PAGE_MODE.DOUBLE) {
            final RarEntryFile header2 = position + 1 < mCurrentHeaders.size() ? mCurrentHeaders.get(position + 1) : null;
            return new BookReadItemLoadObject(header.getName(), new MRarArchiveFile[]{new MRarArchiveFile(header, mArchive), header2 != null ? new MRarArchiveFile(header2, mArchive) : null});
        }
        return new BookReadItemLoadObject(header.getName(), new MRarArchiveFile(header, mArchive));
    }

    public RarFile getArchive() {
        return mArchive;
    }

}
