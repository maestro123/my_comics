package maestro.comics.read;

import android.util.Log;
import maestro.comics.MZipArchiveFile;
import maestro.comics.Settings;
import maestro.comics.Utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by Artyom on 15.08.2014.
 */
public class ZipBookReader extends BaseBookReader {

    public static final String TAG = ZipBookReader.class.getSimpleName();

    private static volatile ZipBookReader instance;

    public static synchronized ZipBookReader getInstance() {
        ZipBookReader localInstance = instance;
        if (localInstance == null)
            synchronized (ZipBookReader.class) {
                localInstance = instance;
                if (localInstance == null) {
                    localInstance = instance = new ZipBookReader();
                }
            }
        return localInstance;
    }

    private ZipFile mArchive;
    private ArrayList<ZipEntry> mEntries;

    @Override
    public void loadBook(String path) {
        try {
            mArchive = new ZipFile(new File(path));
            mEntries = new ArrayList<ZipEntry>();
            Enumeration<? extends ZipEntry> entryEnumeration = mArchive.entries();
            for (Enumeration<? extends ZipEntry> e = mArchive.entries(); e.hasMoreElements(); ) {
                ZipEntry entry = e.nextElement();
                Log.e(TAG, "entry name = " + entry.getName());
                if (entry.getName().toLowerCase().matches(Utils.IMAGE_PATTERN)) {
                    mEntries.add(entry);
                }
            }
            Collections.sort(mEntries, new Comparator<ZipEntry>() {
                @Override
                public int compare(ZipEntry lhs, ZipEntry rhs) {
                    return lhs.getName().compareTo(rhs.getName());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getCount(Settings.PAGE_MODE page_mode) {
        if (mEntries != null && mEntries.size() > 0) {
            switch (page_mode) {
                case DOUBLE:
                    return (int) Math.ceil(mEntries.size() / 2f);
                case SINGLE:
                    return mEntries.size();
            }
        }
        return 0;
    }

    public InputStream getStreamForObject(ZipEntry entry) throws IOException {
        return mArchive.getInputStream(entry);
    }

    public ArrayList<ZipEntry> getEntries() {
        return mEntries;
    }

    @Override
    public BookReadItemLoadObject getLoadObjectAtPosition(Settings.PAGE_MODE pageMode, int position) {
        if (pageMode == Settings.PAGE_MODE.DOUBLE) {
            position *= 2;
        }
        final ZipEntry header = mEntries.get(position);
        if (pageMode == Settings.PAGE_MODE.DOUBLE) {
            final ZipEntry header2 = position + 1 < mEntries.size() ? mEntries.get(position + 1) : null;
            return new BookReadItemLoadObject(header.getName(), new MZipArchiveFile[]{new MZipArchiveFile(header, mArchive),
                    header2 != null ? new MZipArchiveFile(header2, mArchive) : null});
        }
        return new BookReadItemLoadObject(header.getName(), new MZipArchiveFile(header, mArchive));
    }


    @Override
    public void close() {
        try {
            mArchive.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
