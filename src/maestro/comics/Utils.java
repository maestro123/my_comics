package maestro.comics;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import maestro.comics.data.BookManager;
import maestro.rar.RarEntryFile;
import maestro.rar.RarFile;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by Artyom on 31.07.2014.
 */
public class Utils {

    public static final String TAG = Utils.class.getSimpleName();

    public static final String CBR = "^.+(cbr)$";
    public static final String CBZ = "^.+(cbz)$";
    public static final String COMICS_PATTERN = "^.+(cbz|cbr)$";
    public static final String ZIP_PATTERN = "^.+(rar|zip)$";
    public static final String IMAGE_PATTERN = "^.+(jpg|jpeg)$";

    public static final String MIM_COMICS = "mim_comics";

    public static boolean isComicsBook(MFile file) {
        return isComicsBook(file.getName());
    }

    public static boolean isComicsBook(File file) {
        return isComicsBook(file.getName());
    }

    public static boolean isComicsBook(String path) {
        return path.toLowerCase().matches(COMICS_PATTERN);
    }

    public static boolean isArchive(MFile file) {
        return isArchive(file.getName());
    }

    public static boolean isArchive(File file) {
        return isArchive(file.getName());
    }

    public static boolean isArchive(String name) {
        return name != null ? name.toLowerCase().matches(ZIP_PATTERN) : false;
    }

    public static boolean isImage(String name) {
        return name.toLowerCase().matches(IMAGE_PATTERN);
    }

    public static MFile getMFileByPath(String path) {
        //TODO: create symbol for file in archive
        File file = new File(path);
        return new MJavaFile(file, file.getParentFile());
    }

    public static MFile[] toMFiles(List<File> files) {
        final ArrayList<MFile> out = new ArrayList<MFile>();
        if (files != null && files.size() > 0) {
            for (File file : files) {
                out.add(new MJavaFile(file, file.getParentFile()));
            }
        }
        sort(out);
        return out.toArray(new MFile[out.size()]);
    }

    public static MFile[] collectMFiles(Object root) {
        if (root instanceof String) {
            File file = new File((String) root);
            if (isArchive(file.getName())) {
                return collectMFileFromArchive(file, false);
            }
            root = file;
        }
        ArrayList<MFile> outFiles = new ArrayList<MFile>();
        if (root instanceof File) {
            Log.e(TAG, "search path: " + ((File) root).getPath());
            File[] files = ((File) root).listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    return pathname.isDirectory()
                            || pathname.getName().toLowerCase().matches(COMICS_PATTERN);
                }
            });
            if (files != null && files.length > 0)
                for (File file : files) {
                    outFiles.add(new MJavaFile(file, file.getParentFile()));
                }
        } else if (root instanceof RarFile) {
            List<RarEntryFile> rarEntryFiles = ((RarFile) root).list();
            for (RarEntryFile entryFile : rarEntryFiles) {
                outFiles.add(new MRarArchiveFile(entryFile, (RarFile) root));
            }
        }
        sort(outFiles);
        return outFiles.toArray(new MFile[outFiles.size()]);
    }

    public static void sort(ArrayList<MFile> mFiles) {
        ArrayList<MFile> folders = new ArrayList<>();
        ArrayList<MFile> files = new ArrayList<>();
        for (MFile file : mFiles) {
            if (file.isDirectory()) {
                folders.add(file);
            } else {
                files.add(file);
            }
        }
        MFileComparator comparator = new MFileComparator();
        Collections.sort(folders, comparator);
        Collections.sort(files, comparator);
        mFiles.clear();
        mFiles.addAll(folders);
        mFiles.addAll(files);
    }

    public static final MFile[] collectMFileFromArchive(File file, boolean availableOnly) {
        ArrayList<MFile> outFiles = new ArrayList<MFile>();
        if (file.getName().matches(CBZ)) {
            try {
                ZipFile archive = new ZipFile(file);
                Enumeration<? extends ZipEntry> values = archive.entries();
                while (values.hasMoreElements()) {
                    ZipEntry entry = values.nextElement();
                    if (!availableOnly || (availableOnly && isImage(entry.getName())))
                        outFiles.add(new MZipArchiveFile(entry, archive));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (file.getName().matches(CBR)) {
            try {
                RarFile rarFile = new RarFile(file);
                List<RarEntryFile> rarEntryFiles = rarFile.list();
                for (RarEntryFile entryFile : rarEntryFiles) {
                    if (!availableOnly || (availableOnly && isImage(entryFile.getName())))
                        outFiles.add(new MRarArchiveFile(entryFile, rarFile));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Collections.sort(outFiles, new MFileComparator());
        return outFiles.toArray(new MFile[outFiles.size()]);
    }

    public static String encodePath(String path) {
        try {
            return "'" + path + "'";
        } catch (Exception e) {
            e.printStackTrace();
            return path;
        }
    }

    public static String decodePath(String path) {
        try {
            return path.substring(1, path.length() - 1);
        } catch (Exception e) {
            e.printStackTrace();
            return path;
        }
    }

    public static boolean isRar(String path) {
        if (path == null)
            return false;
        String[] parts = path.split("[/]");
        return parts[parts.length - 1].toLowerCase().endsWith(".cbr");
    }

    public static BookFile makeBookFile(String path) {
        return makeBookFile(getMFileByPath(path));
    }

    public static BookFile makeBookFile(MFile file) {
        if (file.getName().toLowerCase().matches(COMICS_PATTERN)) {
            BookFile bookFile = BookManager.getInstance().getBookByPath(file.getPath());
            if (bookFile == null) {
                bookFile = new BookFile(file);
            }
            return bookFile;
        }
        return null;
    }

    public static String makeReadProgress(String progress, Context context) {
        final String[] items = progress.split("[/]");
        final int read = Integer.valueOf(items[0]);
        final int total = Integer.valueOf(items[1]);
        return Math.round(((float) read / total) * 100) + "%" + (context != null ? " " + context.getString(R.string.read) : "");
    }

    public static void prepareEmptyView(Fragment fragment, int buttonText) {
        ((TextView) fragment.getView().findViewById(R.id.empty_title)).setTypeface(Typefacer.rCondensedRegular);
        final Button mEmptyButton = (Button) fragment.getView().findViewById(R.id.empty_button);
        if (buttonText == -1) {
            mEmptyButton.setVisibility(View.GONE);
        } else {
            mEmptyButton.setTypeface(Typefacer.rCondensedRegular);
            mEmptyButton.setText(buttonText);
        }
    }

    public static final class FileHeaderComparator implements Comparator<RarEntryFile> {

        @Override
        public int compare(RarEntryFile fileHeader, RarEntryFile fileHeader2) {
            return fileHeader.getName().compareTo(fileHeader2.getName());
        }
    }

    public static final class MFileComparator implements Comparator<MFile> {
        @Override
        public int compare(MFile mFile, MFile mFile2) {
            return mFile.getName().compareTo(mFile2.getName());
        }
    }

}
