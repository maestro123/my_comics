package maestro.comics;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by Artyom on 31.07.2014.
 */
public class MZipArchiveFile implements MFile {

    public ZipEntry entry;
    public ZipFile parent;

    public MZipArchiveFile(ZipEntry entry, ZipFile parent) {
        this.entry = entry;
        this.parent = parent;
    }

    @Override
    public String getName() {
        return entry.getName();
    }

    @Override
    public String getKey() {
        return null;
    }

    @Override
    public String getPath() {
        return null;
    }

    @Override
    public Object getParent() {
        return parent;
    }

    @Override
    public boolean isDirectory() {
        return false;
    }

    @Override
    public InputStream getStream() {
        try {
            return parent.getInputStream(entry);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public long getSize() {
        return entry.getSize();
    }

    @Override
    public long getTime() {
        return entry.getTime();
    }


}
