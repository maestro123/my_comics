package maestro.comics;

import android.app.Application;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;
import android.util.TypedValue;
import com.dream.android.mim.MIM;
import com.dream.android.mim.MIMColorAnalyzer;
import com.dream.android.mim.MIMManager;
import maestro.comics.data.BookManager;
import maestro.comics.data.ComicsDBHelper;
import maestro.comics.read.MComicsPageMaker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by artyom on 8/11/14.
 */
public class ComicsApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Thread.setDefaultUncaughtExceptionHandler(new MExceptionHandler(Environment.getExternalStorageDirectory().getPath() + "/" + "MY COMICS"));
        Typefacer.getInstance().init(getApplicationContext());
        Settings.getInstance().init(getApplicationContext());
        ComicsDBHelper.getInstance().init(getApplicationContext());
        SVGInstance.getInstance().init(getApplicationContext());
        BookManager.getInstance().init(getApplicationContext());
        BookManager.getInstance().start();

        final int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 80, getResources().getDisplayMetrics());
        final int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 120, getResources().getDisplayMetrics());

        MIMManager.getInstance().addMIM(Utils.MIM_COMICS, new MIM(this)
                .analyzer(new MIMColorAnalyzer() {
                    @Override
                    public Integer[] analyze(Bitmap bitmap) {


                        final int width = bitmap.getWidth();
                        final int height = bitmap.getHeight();
                        final int maxSquare = 30;

                        final HashMap<Integer, AtomicInteger> colors = new HashMap<>();
                        for (int i = 0; i < width && i < maxSquare; i++) {
                            for (int j = 0; j < height && j < maxSquare; j++) {
                                int color = bitmap.getPixel(i, j);
                                if (!colors.containsKey(color)) {
                                    colors.put(color, new AtomicInteger(1));
                                } else {
                                    colors.get(color).incrementAndGet();
                                }
                            }
                        }

                        //Bottom right corner
                        for (int i = width - 1; i > -1 && i > width - 1 - maxSquare; i--) {
                            for (int j = height - 1; j > -1 && j < height - 1 - maxSquare; j--) {
                                int color = bitmap.getPixel(i, j);
                                if (!colors.containsKey(color)) {
                                    colors.put(color, new AtomicInteger(1));
                                } else {
                                    colors.get(color).incrementAndGet();
                                }
                            }
                        }

                        ArrayList<Integer> keys = new ArrayList<>();
                        keys.addAll(colors.keySet());

                        Collections.sort(keys, new Comparator<Integer>() {
                            @Override
                            public int compare(Integer lhs, Integer rhs) {
                                return lhs.intValue() - rhs.intValue();
                            }
                        });

                        return keys.toArray(new Integer[keys.size()]);
                    }
                })
                .maker(new MComicsPageMaker()).size(width, height));

    }

}
