package maestro.comics;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Created by Artyom on 31.07.2014.
 */
public class MJavaFile implements MFile {

    private File file;
    private Object parent;

    public MJavaFile(File file, Object parent) {
        this.file = file;
        this.parent = parent;
    }

    @Override
    public String getName() {
        return file.getName();
    }

    @Override
    public String getKey() {
        return file.getPath();
    }

    @Override
    public String getPath() {
        return file.getPath();
    }

    @Override
    public Object getParent() {
        return parent;
    }

    @Override
    public boolean isDirectory() {
        return file.isDirectory();
    }

    @Override
    public InputStream getStream() {
        try {
            return new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public long getSize() {
        return file.length();
    }

    @Override
    public long getTime() {
        return file.lastModified();
    }
}
