package maestro.comics;

import java.io.InputStream;

/**
 * Created by Artyom on 31.07.2014.
 */
public interface MFile {

    public String getName();

    public String getKey();

    public String getPath();

    public Object getParent();

    public long getSize();

    public long getTime();

    public boolean isDirectory();

    public InputStream getStream();

}
