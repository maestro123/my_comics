package maestro.comics;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.dream.android.mim.ImageLoadObject;
import com.dream.android.mim.MIM;
import com.dream.android.mim.RecyclingImageView;
import maestro.comics.data.BookManager;
import maestro.comics.dialogs.BaseDialogFragment;
import maestro.comics.model.BookCollection;
import maestro.comics.ui.CollectionSelectDialog;

import java.util.ArrayList;

/**
 * Created by Artyom on 15.08.2014.
 */
public class ComicPreviewFragment extends BaseDialogFragment implements AdapterView.OnItemClickListener,
        AbsListView.OnScrollListener, CollectionSelectDialog.OnCollectionSelectListener {

    public static final String TAG = ComicPreviewFragment.class.getSimpleName();

    private static final String PARAM_COMIC_PATH = "param_comic_path";

    private static final int ACTION_ADD_TO_COLLECTION = 0;

    public static final ComicPreviewFragment makeInstance(String path) {
        ComicPreviewFragment fragment = new ComicPreviewFragment();
        Bundle args = new Bundle(1);
        args.putString(PARAM_COMIC_PATH, path);
        fragment.setArguments(args);
        return fragment;
    }

    private View mTitleBackground, headerView;
    private ListView mList;
    private RecyclingImageView mImage;
    private TextView txtTitle;
    private ArrayList<ActionItem> mActionItems = new ArrayList<>();
    private ActionAdapter mAdapter;
    private MIM mim;
    private ImageLoadObject loadObject;
    private BookFile mBook;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.comic_preview_view, null);
        mTitleBackground = v.findViewById(R.id.dialog_header_background);
        mList = (ListView) v.findViewById(R.id.list);
        mList.setOnScrollListener(this);
        mList.setOnItemClickListener(this);
        mList.setDivider(new ColorDrawable(Color.parseColor("#eaeaea")));
        txtTitle = (TextView) v.findViewById(R.id.title);
        txtTitle.setTypeface(Typefacer.rCondensedRegular);
        headerView = inflater.inflate(R.layout.comic_preview_header, null);
        mImage = (RecyclingImageView) headerView.findViewById(R.id.image);
        mList.addHeaderView(headerView);
        mTitleBackground.setAlpha(0);
        return v;
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (mList.getChildCount() > 0) {
            if (headerView.getTop() * -1 <= headerView.getHeight()) {
                headerView.setTranslationY(headerView.getTop() * -1 * 0.5f);
                mTitleBackground.setAlpha((float) headerView.getTop() * -1 / headerView.getHeight());
            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mBook = BookManager.getInstance().getBookByPath(getArguments().getString(PARAM_COMIC_PATH));
        txtTitle.setText(mBook.MFile.getName());
        prepareAction();
        mList.setAdapter(mAdapter = new ActionAdapter(getActivity()));
        mImage.setLoadObject(MIM.by(Utils.MIM_COMICS).of(mBook.MFile.getKey() + "_big").object(mBook));
    }

    @Override
    public void onDestroy() {
        mImage.setLoadObject(null);
        super.onDestroy();
    }

    final void prepareAction() {
        mActionItems.add(new ActionItem(ACTION_ADD_TO_COLLECTION, getString(R.string.add_to_collection)));
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (id == ACTION_ADD_TO_COLLECTION) {
            new CollectionSelectDialog().show(getChildFragmentManager(), CollectionSelectDialog.TAG);
        }
    }

    @Override
    public void onCollectionSelect(BookCollection collection) {
        BookManager.getInstance().addToCollection(mBook, collection);
    }

    private static final class ActionItem {

        public String Text;
        public int ActionId;
        private int IconId = -1;
        private boolean isSelected;

        public ActionItem(int id, String text) {
            ActionId = id;
            Text = text;
        }

        public ActionItem(int id, String text, int iconId) {
            this(id, text);
            IconId = iconId;
        }

    }

    private class ActionAdapter extends BaseAdapter {

        private LayoutInflater mInflater;

        public ActionAdapter(Context context) {
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mActionItems != null ? mActionItems.size() : 0;
        }

        @Override
        public ActionItem getItem(int position) {
            return mActionItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return mActionItems.get(position).ActionId;
        }

        @Override
        public View getView(int position, View v, ViewGroup parent) {
            Holder h;
            if (v == null) {
                h = new Holder();
                v = mInflater.inflate(R.layout.comic_preview_list_item_view, null);
                h.Icon = (ImageView) v.findViewById(R.id.icon);
                h.Text = (TextView) v.findViewById(R.id.text);
                h.Text.setTypeface(Typefacer.rCondensedRegular);
                v.setTag(h);
            } else {
                h = (Holder) v.getTag();
            }
            ActionItem item = getItem(position);
            h.Text.setText(item.Text);
            if (item.IconId != -1) {
                h.Icon.setVisibility(View.GONE);
            } else {
                h.Icon.setVisibility(View.VISIBLE);
                h.Icon.setImageResource(item.IconId);
            }
            return v;
        }

        class Holder {
            ImageView Icon;
            TextView Text;
        }

    }

}
