package maestro.comics;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.view.*;
import android.widget.*;
import com.dream.android.mim.MIM;
import maestro.comics.data.BookManager;
import maestro.comics.read.ReadActivity;
import java.util.ArrayList;


public class FileBrowserActivity extends FragmentActivity implements AdapterView.OnItemClickListener, BookManager.OnBookManagerEventListener {

    public static final String TAG = FileBrowserActivity.class.getSimpleName();

    private static final String SAVED_HISTORY = "saved_history";
    private static final String PARAM_PATH = "param_path";
    private static final String PARAM_FROM_DATABASE = "param_from_database";

    private static final int FILES_LOADER_ID = FileBrowserActivity.class.getSimpleName().hashCode();

    private GridView mList;
    private FileAdapter mAdapter;
    private ArrayList<String> mHistory = new ArrayList<String>();

//    private boolean fromDatabase = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mList = (GridView) findViewById(R.id.list);
        mList.setOnItemClickListener(this);
        mList.setAdapter(mAdapter = new FileAdapter(this));
        if (savedInstanceState == null) {
            mHistory.add(Environment.getExternalStorageDirectory().getPath());
        } else {
            mHistory = savedInstanceState.getStringArrayList(SAVED_HISTORY);
        }
//        startLoad(true);
        BookManager.getInstance().attachListener(TAG, this);
        updateBooks();
    }

    @Override
    protected void onDestroy() {
        BookManager.getInstance().detachListener(TAG);
        super.onDestroy();
    }

    @Override
    public void onBookManagerEvent(BookManager.BOOK_MANAGER_EVENT event, Object object) {
        updateBooks();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mAdapter.notifyDataSetChanged();
    }

    public void updateBooks() {
        if (mAdapter != null) {
//            mAdapter.update(BookManager.getInstance().getBooks());
        }
    }

    //    public void startLoad(boolean fromDatabase) {
//        Bundle args = new Bundle(1);
//        args.putBoolean(PARAM_FROM_DATABASE, fromDatabase);
//        this.fromDatabase = fromDatabase;
//        if (getSupportLoaderManager().getLoader(FILES_LOADER_ID) != null) {
//            getSupportLoaderManager().restartLoader(FILES_LOADER_ID, args, this);
//        } else {
//            getSupportLoaderManager().initLoader(FILES_LOADER_ID, args, this);
//        }
//    }

    @Override
    public void onBackPressed() {
        if (mHistory.size() > 1) {
            mHistory.remove(mHistory.size() - 1);
//            startLoad(false);
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArrayList(SAVED_HISTORY, mHistory);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        BookFile file = (BookFile) adapterView.getItemAtPosition(i);
        if (file.MFile.isDirectory()) {
            if (!mHistory.contains(file.MFile.getPath())) {
                mHistory.add(file.MFile.getPath());
//                startLoad(false);
            }
        } else if (Utils.isComicsBook(file.MFile) || Utils.isArchive(file.MFile)) {
            openBook(file.MFile.getPath());
        } else if (Utils.isArchive(file.MFile)) {
            mHistory.add(file.MFile.getName());
        }
    }

//    @Override
//    public Loader<BookFile[]> onCreateLoader(int i, Bundle bundle) {
//        return new FilesLoader(this, bundle != null ? bundle.getString(PARAM_PATH) : null,
//                bundle != null ? bundle.getBoolean(PARAM_FROM_DATABASE) : false);
//    }
//
//    @Override
//    public void onLoadFinished(Loader<BookFile[]> loader, BookFile[] files) {
//        Log.e(TAG, "onLoadFinished = " + files);
//        if (mAdapter != null) {
//            mAdapter.update(files);
//        }
//        if (fromDatabase)
//            startLoad(false);
//    }
//
//    @Override
//    public void onLoaderReset(Loader<BookFile[]> loader) {
//
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("Test");
        menu.add("Wiki");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void openBook(String path) {
        Intent i = new Intent(this, ReadActivity.class);
        i.putExtra(ReadActivity.PARAM_PATH, path);
        startActivityForResult(i, 0);
    }

//    public static final class FilesLoader extends AsyncTaskLoader<BookFile[]> {
//
//        private Object root;
//        private boolean fromDatabase;
//
//        public FilesLoader(Context context, Object root, boolean fromDatabase) {
//            super(context);
//            this.root = root;
//            this.fromDatabase = fromDatabase;
//        }
//
//        @Override
//        public BookFile[] loadInBackground() {
//            if (fromDatabase) return ComicsDBHelper.getInstance().loadBooks();
//            MFile[] files = null;
//            if (root != null) {
//                files = Utils.collectMFiles(root);
//            } else {
//                files = Utils.toMFiles(ObjectFinder.find(Utils.COMICS_PATTERN));
//            }
//            if (files != null && files.length > 0) {
//                final ArrayList<BookFile> outFiles = new ArrayList<BookFile>();
//                final ComicsDBHelper helper = ComicsDBHelper.getInstance();
//                for (MFile file : files) {
//                    if (!helper.haveBookWithPath(file.getPath())) {
//                        BookFile bookFile = new BookFile(file);
//                        helper.addRow(bookFile);
//                        outFiles.add(bookFile);
//                    }
//                }
//                return outFiles.toArray(new BookFile[outFiles.size()]);
//            }
//            return null;
//        }
//
//        @Override
//        protected void onStartLoading() {
//            super.onStartLoading();
//            forceLoad();
//        }
//    }

    private class FileAdapter extends BaseAdapter {

        private BookFile[] mItems;
        private LayoutInflater mInflater;

        public FileAdapter(Context context) {
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public void update(BookFile[] files) {
            mItems = files;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mItems != null ? mItems.length : 0;
        }

        @Override
        public BookFile getItem(int i) {
            return mItems[i];
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int position, View v, ViewGroup viewGroup) {
            Holder h;
            if (v == null) {
                h = new Holder();
                v = mInflater.inflate(R.layout.book_item_view, null);
                h.Title = (TextView) v.findViewById(R.id.title);
                h.Image = (ImageView) v.findViewById(R.id.image);
                h.Additional = (TextView) v.findViewById(R.id.additional);
                v.setTag(h);
            } else {
                h = (Holder) v.getTag();
            }
            BookFile file = mItems[position];
            h.Title.setText(file.MFile.getName());
            MIM.by(Utils.MIM_COMICS).to(h.Image, file.MFile.getPath()).object(file).async();
//            if (file.ReadProgress != null) {
//                h.Additional.setVisibility(View.VISIBLE);
//                h.Additional.setText(file.ReadProgress);
//            } else {
//                h.Additional.setVisibility(View.GONE);
//            }

            h.Title.setVisibility(View.GONE);
            h.Additional.setVisibility(View.GONE);
            return v;
        }

        class Holder {
            TextView Title;
            TextView Additional;
            ImageView Image;
        }

    }

}
