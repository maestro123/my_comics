package maestro.comics;

import maestro.comics.model.BookCollection;

/**
 * Created by artyom on 8/11/14.
 */
public class BookFile {

    public long Id;
    public MFile MFile;
    public int Position;
    public String ReadProgress;
    public long ParentId = -1;
    public BookCollection Collection;

    public BookFile(MFile file) {
        MFile = file;
    }

    public BookFile(long id, MFile file, int position, String readProgress, long parentId) {
        Id = id;
        MFile = file;
        Position = position;
        ReadProgress = readProgress;
        ParentId = parentId;
    }

    public void setCollection(BookCollection collection){
        Collection = collection;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof BookFile) {
            BookFile bookFile = (BookFile) o;
            return MFile.getPath().equals(bookFile.MFile.getPath());
        }
        return super.equals(o);
    }
}
